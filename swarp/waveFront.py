#!/bin/python

#/*##########################################################################
#
# SWaRP: Speckle Wavefront Reconstruction Package 
#
# Copyright (c) 2016-2018 European Synchrotron Radiation Facility
#
# This file is part of the SWaRP Speckle Wavefront Reconstruction Package
# developed at the ESRF by the staff of BM05 as part of EUCALL WP7:PUUCA.
#
# This project has received funding from the European Union’s Horizon 2020 
# research and innovation programme under grant agreement No 654220.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
#############################################################################*/

__author__ = 'Ruxandra Cojocaru, Sebastien Berujon'
__contact__ = 'cojocaru@esrf.fr; sebastien.berujon@esrf.fr'
__license__ = 'MIT'
__copyright__ = 'European Synchrotron Radiation Facility, Grenoble, France'
__date__ = '27/08/2018'

import sys
import os
from os import walk
import math
import numpy as np
import scipy.ndimage.filters
import scipy.interpolate
# Remove X11 dependency                                                                                                              
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
from mpl_toolkits.mplot3d import Axes3D
from PIL import Image
import h5py
import time
import datetime
import logging
from logging.handlers import RotatingFileHandler
from logging import handlers
# Non-standard python libraries
from EdfFile import EdfFile
import func
import norm_xcorr
import g2s

def main():
    
    # Get time stamp
    ts = time.time()
    dt = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d_%H:%M:%S')

    # Initializing logging
    log = logging.getLogger('')
    log.setLevel(logging.INFO)
    format = logging.Formatter('%(levelname)s: %(message)s')
    
    ch = logging.StreamHandler(sys.stdout)
    ch.setFormatter(format)
    log.addHandler(ch)

    # Read images and ini file paths from command line

    error_msg = ('Script call must be: script.py path_image1 path_image2 ' 
                 'file.ini [...]')

    if(not func.test_err(len(sys.argv) >= 3, error_msg)):
        path_img1 = os.path.abspath(sys.argv[1])
        path_img2 = os.path.abspath(sys.argv[2])
        path_ini = os.path.abspath(sys.argv[3])
    
    for file in [path_img1, path_img2, path_ini]:
        error_msg = 'File ' + str(file) + 'does not exist on disk'
        func.test_err(os.path.exists(file), error_msg)

    # Read input parameters from ini file
    section = 'waveFront.config'
    param = func.check_input(section, path_ini, dt)
    
    # Start also saving log information to file
    fh = handlers.RotatingFileHandler(os.path.join(param['dir_out'], 
                                      'waveFront_' + dt + '.log'), 
                                      maxBytes=(1048576*5), backupCount=7)
    fh.setFormatter(format)
    log.addHandler(fh)
        
    # Save data and/or plots?
    save_data = True
    save_plots = True
    #--------------
    
    # MUST CORRECT THIS SECTION
    # WHAT HAPPENS IF undistort is ON but roi_default_dd is none???

    # Other parameters
    sandp_filt = 0
    filtSz = 7
    avgFilt = 0
    gaussFilt = 0

    logging.info('Pixel sizes: detector 1 = ' + str(param['pix_size1']) 
                + ', detector 2 = ' + str(param['pix_size2']))

    # Some useful values:

    # lambda in micrometers (um)
    wave_length = 12.398 / param['energy'] * float('1e-4') 
    kw = 2 * math.pi / wave_length
    rad_th = 3000
    delta = float('1.5138e-6')

    # Open files for dark and flat field images

    # Obtain image for Dark Field correction
    [dark1, have_dark1, dark_avg1] = func.compute_corr_field(
                                     param['path_dark1'], 
                                     param['prefix_dark1'], 
                                     param['file_type'], 
                                     'Dark', 'median')
                                    
    if (have_dark1):
        func.save_plot(dark1, 'dark', param['dir_out'], sub_dir=param['subdir_fig'])
                                    
    if(param['diff_wf'] == 0):
        [dark2, have_dark2, dark_avg2] = func.compute_corr_field(
                                         param['path_dark2'], 
                                         param['prefix_dark2'], 
                                         param['file_type'], 
                                         'Dark', 'median')
    else:
        [dark2, have_dark2, dark_avg2] = [dark1, have_dark1, dark_avg1]

    # Obtain image for Flat Field correction
    [flat1, have_flat1, flat_avg1] = func.compute_corr_field(
                                     param['path_flat1'], 
                                     param['prefix_flat1'], 
                                     param['file_type'], 
                                     'Flat', 'average')
    
    if (have_flat1):
        func.save_plot(flat1, 'flat', param['dir_out'], sub_dir=param['subdir_fig'])
    
    if(param['diff_wf'] == 0):
        [flat2, have_flat2, flat_avg2] = func.compute_corr_field(
                                         param['path_flat2'], 
                                         param['prefix_flat2'], 
                                         param['file_type'], 
                                         'Flat', 'average')
    else:
        [flat2, have_flat2, flat_avg2] = [flat1, have_flat1, flat_avg1]

    # START HERE
        
    logging.info('Start pair processing:')

    file_name1 = os.path.basename(path_img1)
    file_name2 = os.path.basename(path_img2)
    
    logging.info([file_name1, file_name2])

    if (param['file_type'].lower() == 'edf'):
        image_file1 = (EdfFile(path_img1)
                       .GetData(0).astype('float32'))
        image_file2 = (EdfFile(path_img2)
                       .GetData(0).astype('float32'))
        func.save_plot(image_file1, 'img1', param['dir_out'], sub_dir=param['subdir_fig'])
        func.save_plot(image_file2, 'img2', param['dir_out'], sub_dir=param['subdir_fig'])
    elif (param['file_type'].lower() == 'tif' 
          or param['file_type'].lower() == 'tiff'):
        im1 = Image.open(path_img1)
        image_file1 = np.array(im1).astype('float32')
        im2 = Image.open(path_img2)
        image_file2 = np.array(im2).astype('float32')
    elif (param['file_type'].lower() == 'h5' 
          or param['file_type'].lower() == 'hdf5'):
        # create File Object which acts as a python dictionary
        im1 = h5py.File(path_img1, 'r')
        im2 = h5py.File(path_img2, 'r')
        # List all keys
        logging.info('Keys: %s' % im1.keys())
        logging.info('Keys: %s' % im2.keys())
        a_group_key1 = list(im1.keys())[0]
        a_group_key2 = list(im2.keys())[0]
        # Get the data
        data1 = list(im1[a_group_key1])
        data2 = list(im1[a_group_key2])
        image_file1 = np.array(data1)
        image_file2 = np.array(data2)
    else:
        ff_err = ('Error: unknown file format. Check that your files '
                  'are EDF, TIF/TIFF or h5.')
        test_err(False, ff_err)

    logging.info('Using file ' + str(path_img1) + ' as Image 1.')
    logging.info('Using file ' + str(path_img2) + ' as Image 2.')
    
    # Find ROIs for these files
    if (param['roi_mode'] == 'full'):
        param['roi_default1'] = [1, image1.shape(0), 1, image1.shape(1)]
        param['roi_default2'] = [1, image2.shape(0), 1, image2.shape(1)]
        
    if (param['roi_mode'] == 'auto'):
        dark_avg1 = 100
        dark_avg2 = 100
        
        if(have_dark1):
            dark_avg1 = np.average(dark1)
            if(param['diff_wf']):
                dark_avg2 = dark_avg1
        if(have_dark2):
            dark_avg2 = np.average(dark2)
            
        param['roi_default1'] = roi_auto(image1, dark_avg1)
        param['roi_default2'] = roi_auto(image2, dark_avg2)

    logging.info('ROI1: ' + str(param['roi_default1']))
    logging.info('ROI2: ' + str(param['roi_default2']))

    # Perform dark and flat field corrections
    image1 = func.correction(image_file1, 
                             have_flat1, 
                             flat1, 
                             have_dark1, 
                             dark1)

    if(param['diff_wf'] == 0):
        image2 = func.correction(image_file2, 
                                 have_flat2, 
                                 flat2, 
                                 have_dark2, 
                                 dark2)
    else:
        image2 = func.correction(image_file2, 
                                 have_flat1, 
                                 flat1, 
                                 have_dark1, 
                                 dark1)
    
    roi_corr1 = param['roi_default1']
    roi_corr2 = param['roi_default2']
    
    if (param['undistort'] == 1):
        image1 = func.undistort(image1, 
                                param['roi_default_dd1'], 
                                param['horz_disto_map1'], 
                                param['vert_disto_map1'])
                                
        func.save_plot(image1, 'img1_after_undistort', param['dir_out'], sub_dir=param['subdir_fig'])
        
        # Recalculate ROI1 and ROI2
        roi_corr1 = func.roi_recalc(param['roi_default1'], param['roi_default_dd2'])
        if(param['diff_wf'] == 0):
            roi_corr2 = func.roi_recalc(param['roi_default1'], param['roi_default_dd2'])
            image2 = func.undistort(image2, 
                                    param['roi_default_dd2'], 
                                    param['horz_disto_map2'], 
                                    param['vert_disto_map2'])
        else:
            roi_corr2 = roi_corr1
            image2 = func.undistort(image2, 
                                    param['roi_default_dd1'], 
                                    param['horz_disto_map1'], 
                                    param['vert_disto_map1'])
                                    
            func.save_plot(image2, 'img2_after_undistort', param['dir_out'], sub_dir=param['subdir_fig'])
            
        logging.info('new ROIs:')
        logging.info('ROI1: ' + str(roi_corr1))
        logging.info('ROI2: ' + str(roi_corr2))

    # Adjust pixel size and ROI according to binning factor
    if(param['under_sample'] > 1):
        param['pix_size1'] = param['pix_size1'] * param['under_sample']
        param['pix_size2'] = param['pix_size2'] * param['under_sample']
        
        roi_corr1 = [x / param['under_sample'] for x in roi_corr1]
        roi_corr2 = [x / param['under_sample'] for x in roi_corr2]

    # Interpolation based on relative difference in pixel size

    interp_img1 = param['pix_size1'] / param['pix_size2']

    roi_adj2 = roi_corr2
    pix_size2_adj = param['pix_size2']

    # Interpolate second image to adjust for different pixel size

    if (interp_img1 != 0.0 and interp_img1 != 1.0 
        and param['diff_wf'] == 0):
        [m2, n2] = image2.shape
        y = np.linspace(0, m2, num = m2)
        x = np.linspace(0, n2, num = n2)
        yi = np.linspace(0, m2, num = round(m2/interp_img1))
        xi = np.linspace(0, n2, num = round(n2/interp_img1))
        f = scipy.interpolate.interp2d(x, y, image2, kind = 'cubic')
        image2 = f(xi, yi).astype('float32')
        pix_size2_adj = param['pix_size2'] * interp_img1
        logging.info('Adjusted pix_size2 = ' + str(pix_size2_adj))
        roi_adj2 = np.round(roi_corr2/interp_img1)
        [m2, n2] = image2.shape

    logging.info('Made corrections when possible.')

    # Apply the various filters to the images
    if (sandp_filt > 0):
        image1 = func.myErosion(image1, sandp_filt, filtSz)
        image2 = func.myErosion(image2, sandp_filt, filtSz)
        
    if (gaussFilt > 0):
        filt_h =  func.f_gaussian(sigma = gaussFilt)
        image1 = scipy.ndimage.correlate(image1, filt_h, mode = 'nearest')
        image2 = scipy.ndimage.correlate(image2, filt_h, mode = 'nearest')
        
    if (avgFilt > 0):
        image1 = func.avg_filter(image1, avgFilt)
        image2 = func.avg_filter(image2, avgFilt)

    logging.info('Applied different filters.')

    # Calculate speckle displacement

    if(param['image_order'] is 1):
        [speck_dis, err_mat, flag] = func.cross_spot(image1, 
                                                     image2, 
                                                     param['roi_mode'], 
                                                     roi_corr1, roi_adj2, 
                                                     param['grid_resol'], 
                                                     param['mode'], 
                                                     param['corr_size'], 
                                                     param['resol_first_pass'], 
                                                     param['half_width_fact'], 
                                                     param['under_sample'], 
                                                     param['dir_out'], 
                                                     file_name1, 
                                                     file_name2)
    else:
        [speck_dis, err_mat, flag] = func.cross_spot(image2, 
                                                     image1, 
                                                     param['roi_mode'], 
                                                     roi_adj2, roi_corr1, 
                                                     param['grid_resol'], 
                                                     param['mode'], 
                                                     param['corr_size'], 
                                                     param['resol_first_pass'], 
                                                     param['half_width_fact'], 
                                                     param['under_sample'], 
                                                     param['dir_out'], 
                                                     file_name1, 
                                                     file_name2)

    logging.info('Out of cross_spot.')

    # Exit if cross-correlation failed
    func.test_err(flag, 'waveFront', 'Cross-correlation failed!')

    (JH, JV) = np.meshgrid(range(speck_dis[0, :, :].shape[1]), 
                           range(speck_dis[0, :, :].shape[0]))

    if(param['image_order']  is 1):
        pix_sizeCorr = ((pix_size2_adj - param['pix_size1']) 
                         / pix_size2_adj)
    else:
        pix_sizeCorr =  ((param['pix_size1'] - pix_size2_adj) 
                          / param['pix_size1'])
        
    speck_dis_v = speck_dis[0, :, :] + JV * pix_sizeCorr
    speck_dis_h = speck_dis[1, :, :] + JH * pix_sizeCorr

    if(len(err_mat) > 0):
        speck_dis_vmasked = np.ma.array(speck_dis_v, mask = False)
        speck_dis_vmasked.mask[np.unravel_index(err_mat[0], 
                                                speck_dis_v.shape)] = True

        speck_dis_v = speck_dis_v - np.mean(speck_dis_vmasked)
        
        speck_dis_hmasked = np.ma.array(speck_dis_h, mask = False)
        speck_dis_hmasked.mask[np.unravel_index(err_mat[0], 
                                                speck_dis_h.shape)] = True

        speck_dis_h = speck_dis_h - np.mean(speck_dis_hmasked)
    else:
        speck_dis_v = speck_dis_v - np.mean(speck_dis_v)
        speck_dis_h = speck_dis_h - np.mean(speck_dis_h)

    # Compute WF at the position of the 1st detector
    speck_dis_v = speck_dis_v * param['pix_size1'] / param['dist']
    speck_dis_h = speck_dis_h * param['pix_size1'] / param['dist']
    
    if (save_data):
        file_v_1 = os.path.join(param['dir_out'], param['subdir_data'],
                   'speck_dis_v_' + file_name1 + '_' + file_name2 + '.dat')
        np.savetxt(file_v_1, speck_dis_v * 1000.0, 
                   fmt = '%.18e', delimiter = '\t', newline = '\n')
        file_h_1 = os.path.join(param['dir_out'], param['subdir_data'],
                   'speck_dis_h_' + file_name1 + '_' + file_name2 + '.dat')
        np.savetxt(file_h_1, speck_dis_h * 1000.0, 
                   fmt = '%.18e', delimiter = '\t', newline = '\n')
               
    if (save_plots):
        func.save_plot(speck_dis_v * 1000.0, 'speck_dis_v_' + file_name1 
                       + '_'  + file_name2, param['dir_out'], 
                       sub_dir=param['subdir_fig'], clabel='urad')    
        func.save_plot(speck_dis_h * 1000.0, 'speck_dis_h_' + file_name1 
                       + '_'  + file_name2, param['dir_out'], 
                       sub_dir=param['subdir_fig'], clabel='urad')
                       
    # CONSIDER CHANGING ACCORDING TO IMAGE ORDER
    # Absorption
    image2[image2 == 0] = 0.000001
    # WORK ON THIS
    absorption = (image1 / image2)
    absorption = func.roi_select(absorption, roi_corr1)

    if (save_plots):
        func.save_plot(absorption, 'absorption', param['dir_out'], 
                       sub_dir=param['subdir_fig'], clabel='absorption')

    # mask
    # Grad2Surf
    if(param['mask_half_size']):
        [big_mask, center_mask] = func.mask_builder(absorption, param['mask_half_size']) # param['mask_half_size']/pix_size1)
        logging.info('Mask center: ' + str(center_mask))
        func.save_plot(big_mask, 'mask_2D', param['dir_out'], 
                       sub_dir=param['subdir_fig'])

    # Wavefront reconstruction (2D integration for the gradients)

    met = '_FC_'

    if(param['integ_meth'] is 1):
        # Frankot-Chellappa
        logging.info('Wavefront reconstruction using Frankot-Chellappa '
              'algorithm.')
        if(param['mask_half_size']):
            phi = func.frankot_chellappa(speck_dis_h * big_mask, speck_dis_v * big_mask)
        else:
            phi = func.frankot_chellappa(speck_dis_h, speck_dis_v)
    elif(param['integ_meth'] is 2):
        # Grad2Surf
        logging.info('Wavefront reconstruction using Grad2Surf algorithm.')
        met = '_g2s_'
        
        (ny, nx) = speck_dis_v.shape
        
        x = np.linspace(0, nx-1, num = nx)
        y = np.linspace(0, ny-1, num = ny)
        
        
        if(param['mask_half_size']):
            phi = g2s.g2s(x, y, speck_dis_h * big_mask, speck_dis_v * big_mask)
        else:
            phi = g2s.g2s(x, y, speck_dis_h, speck_dis_v)
    else:
        logging.info('Wavefront reconstruction using Frankot-Chellappa '
                    'algorithm.')
        if(param['mask_half_size']):
            phi = func.frankot_chellappa(speck_dis_h * big_mask, speck_dis_v * big_mask)
        else:
            phi = func.frankot_chellappa(speck_dis_h, speck_dis_v)

        (ny, nx) = speck_dis_v.shape
        
        x = np.linspace(0, nx-1, num = nx)
        y = np.linspace(0, ny-1, num = ny)
        
        # Grad2Surf
        logging.info('Wavefront reconstruction using Grad2Surf algorithm.')
        if(param['mask_half_size']):
            phi2 = g2s.g2s(x, y, speck_dis_h * big_mask, speck_dis_v * big_mask)
        else:
            phi2 = g2s.g2s(x, y, speck_dis_h, speck_dis_v)
            
    fact = 1.0

    if(param['image_order'] is 2):
        fact = -1.0

    phi = fact * phi
    # Remove piston
    if(param['mask_half_size']):
        phi = phi - np.mean(phi[big_mask])
    else:
        phi = phi - np.mean(phi)

    # Displace so that minimum is at approx. 0
    phimin = min(phi.flatten())
    phi = phi - phimin

    if (save_data):
        file_wf = os.path.join(param['dir_out'], param['subdir_data'],
                  'wf' + met + file_name1 + '_' + file_name2 + '.dat')
        np.savetxt(file_wf, phi)

    if (save_plots):
        func.save_plot(phi, '2D', param['dir_out'], 
                       sub_dir=param['subdir_fig'])
        func.save_plot(phi, '3D', param['dir_out'], 
                       sub_dir=param['subdir_fig'], xlabel='x [mm]', ylabel='y [mm]', 
                       zlabel='z [nm]', plot_type='3D', 
                       pixel_size = param['pix_size1'])

    if (param['integ_meth'] is 0):
        phi2 = fact * phi2
        # Remove piston
        if(param['mask_half_size']):
            phi2 = phi2 - np.mean(phi2[big_mask])
        else:
            phi2 = phi2 - np.mean(phi2)
        # Displace so that minimum is at approx. 0
        phi2min = min(phi2.flatten())
        phi2 = phi2 - phi2min

        if (save_data):
            file_wf2 = os.path.join(param['dir_out'], param['subdir_data'],
                      'wf_g2s_' + file_name1 + '_' + file_name2 + '.dat')
            np.savetxt(file_wf, phi2)

        if (save_plots):
            func.save_plot(phi2, '2D_g2s', param['dir_out'], 
                           sub_dir=param['subdir_fig'], clabel = 'z [nm]')
            func.save_plot(phi2, '3D_g2s', param['dir_out'], 
                           sub_dir=param['subdir_fig'], xlabel='x [mm]', ylabel='y [mm]', 
                           zlabel='z [nm]', plot_type='3D', 
                           pixel_size = param['pix_size1'])

if __name__ == '__main__':
    
    main()
