#!/bin/python

#/*##########################################################################
#
# SWaRP: Speckle Wavefront Reconstruction Package 
#
# Copyright (c) 2016-2018 European Synchrotron Radiation Facility
#
# This file is part of the SWaRP Speckle Wavefront Reconstruction Package
# developed at the ESRF by the staff of BM05 as part of EUCALL WP7:PUUCA.
#
# This project has received funding from the European Union’s Horizon 2020 
# research and innovation programme under grant agreement No 654220.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
#############################################################################*/

__author__ = ['Ruxandra Cojocaru', 'Sebastien Berujon']
__contact__ = 'cojocaru@esrf.fr; sebastien.berujon@esrf.fr'
__license__ = 'MIT'
__copyright__ = 'European Synchrotron Radiation Facility, Grenoble, France'
__date__ = '27/08/2018'

import sys
import os
from os import walk
import numpy as np
import math
import scipy.ndimage
import scipy.ndimage.filters
import scipy.interpolate
from scipy.sparse import coo_matrix
from scipy.sparse.linalg import spsolve
from scipy import array
from scipy.sparse.linalg import lsmr
# remove X11 dependency                                                                                                              
import matplotlib as mpl
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
from mpl_toolkits.mplot3d import Axes3D
mpl.use('Agg')
#~ mpl.use('TkAgg')
import matplotlib.pyplot as plt 
import uuid
import operator
import cv2
import itertools
from joblib import Parallel, delayed
import multiprocessing
from PIL import Image
from mpl_toolkits.axes_grid1 import make_axes_locatable
import decimal
import logging
if sys.version_info < (3,):
    import ConfigParser
    from StringIO import StringIO
else:
    import configparser as ConfigParser
from scipy.interpolate import InterpolatedUnivariateSpline
# for exporting to .mat format
import scipy.io as sio
# Non-standard python libraries
from .EdfFile import EdfFile
from . import norm_xcorr as nx
from . import g2s

# String for default values on optional arguments
DEFAULT = uuid.uuid4()

def test_err(assertion, func = 'function', message = 'False assertion', 
    verbose = True):
    """Tests if assertion can be interpreted as False. If this is the  
    case, print an error message and stop the execution of the program.
    """

    result = not bool(assertion)

    if (result):
        if (verbose):
            logging.error('Function ' + func + ': ' + message)
        sys.exit(1)

    return result


def test_warn(assertion, func = 'function', message = 'False assertion', 
    verbose = True):
    """Tests if assertion can be interpreted as False. If this is the
    case, print a warning message, but continue the execution of the 
    program.
    Returns True if problem found. This can be used in an if clause to 
    perform additional actions to compensate for the problem 
    (e.g. asign a default value to some variable)
    """
    
    result = not bool(assertion)

    if (result and verbose):
        logging.warn('Function ' + func + ': ' + message)

    return result
    

def replace_default(key, value, data_type, use_default, default_val, condition, 
                    reason):
    '''Function that deals with replacing inputed values with default values
       when this is needed (and possible)'''
    
    value_out = value
    
    if (use_default):
        if (default_val):
            type_warn = (str(reason) + ' Parameter value for key ' + str(key) 
                         + ' of data type ' + str(data_type) 
                         + ' has been replaced with default value ' 
                         + str(default_val) + ' . Initially inputed value: ' 
                         + str(value))
            test_warn(condition, 'check_key', type_warn)
        value_out = default_val
    else:
        type_err = (str(reason) + ' Parameter value for key ' + str(key) 
                    + ' of data type ' + str(data_type) 
                    + ' is NEEDED and there is no default value for this.'
                    + ' Initially inputed value: ' + str(value))
        test_err(condition, 'check_key', type_err)
        
    return value_out
    
    
def check_key(key, config, section, data_type = 'str', default_val = None, 
              use_default = True, positive = False, acc_val = None):
    """Check if key exists in dictionary. If it does not exist and no 
    default value given, use error function. Otherwise print warning and 
    use default.
    This function will change the input dictionary when needed, as it is
    a mutable object
    If positive is True for a int/float, the code will check if the number is
    positive
    If positive is True for a path, then the code will check if the path
    points to a true location, otherwise it will replace it with None
    """

    # Check if key exists in dictionary. If not, replace with default
    # if possible or call error
    
    reason = 'Key does not exist in INI file.'
    condition = bool(key in config.options(section))
    value = None
    
    if(condition):
        value = config.get(section, key)
    else:
        replace_default(key, value, data_type, use_default, default_val, 
                        condition, reason)

    # Remove leading and final quotes/apostrophes in case they were
    # introduced by the user
    try:
      basestring
    except NameError:
      basestring = str
    
    if(isinstance(value, str) or isinstance(value, basestring)):
        if value.startswith('\"') and value.endswith('\"'):
            value = value[1:-1]
            
        if value.startswith('\'') and value.endswith('\''):
             value = value[1:-1]
             
    # Check if inputed value is None or 'none'
    if (value == 'none'):
        value = None
    
    # Force correct type. If it cannot be done, replace with default
    # if possible or call error            
    try:
        if(value):
            if (data_type == 'float'):
                value = float(value)
            elif (data_type == 'int'):
                value = int(value)
            elif (data_type == 'bool'):
                if (str(config.get(section, key)).lower() == 'true'):
                    value = True
                elif (str(value).lower() == 'false' or int(value) == 0):
                    value = False
                else:
                    value = bool(value)
            elif (data_type == 'mask'):
                
                mask_list = str(value).split()

                mask_err1 = ('The string for ' + key + ' must be \'none\' or '
                            'must contain 1 or 2 positive integers separated '
                            'by a  space')

                mask_err2 = ('At least one of the values in the inputed '
                            'string for ' + key + ' cannot be interpreted as '
                            'INTEGER')

                mask_err3 = ('The values in the inputed string for '
                            + key + ' must all be POSITIVE INTEGERS')

                test_err(len(mask_list) == 1 or len(mask_list) == 2, 
                         'check_key', mask_err1)

                try: 
                    mask_list = [int(i) for i in mask_list]
                    value = mask_list
                except:
                    test_err(False, 'check_key', mask_err2)
                    
                test_err(any(item > 0 for item in mask_list), 'check_key', 
                         mask_err3)
                
            elif (data_type == 'roi'):
                roi_list = value.split()
                roi_err1 = ('The string for ' + key + ' must be \'none\' or '
                            'must contain 4 positive integers separated by a '
                            'space using this format \'xmin xmax ymin ymax\'')
                roi_err2 = ('At least one of the 4 values in the inputed '
                            'string for ' + key + ' cannot be interpreted as '
                            'INTEGER')
                roi_err3 = ('The 4 values in the inputed string for '
                            + key + ' must all be POSITIVE INTEGERS')
                roi_err4 = 'In ' + key +  ', check if xmin < xmax'
                roi_err5 = 'In ' + key +  ', check if ymin < ymax'
                roi_err6 = 'In ' + key +  ', check if ymin < ymax'

                test_err(len(roi_list) == 4, 'check_key', roi_err1)
                
                try: 
                    roi_list = [int(i) for i in roi_list]
                    value = roi_list
                except:
                    test_err(False, 'check_key', roi_err2)
                    
                test_err(any(item > 0 for item in roi_list), 'check_key', 
                         roi_err3)
                test_err(roi_list[0] < roi_list[1], 'check_key', roi_err4)
                test_err(roi_list[2] < roi_list[3], 'check_key', roi_err5)
                
            elif (data_type == 'path'):
                value = os.path.abspath(value)
                if (not os.path.exists(value)):
                    if(positive):
                        warn = ('Path in key ' + str(key) + ' with value: '
                                + str(value) + ' does not exist on disk. '
                                + 'Replacing with None.')
                        test_warn(False, 'check_input', warn)
                        value = None
                    else:
                        # Create path, check write permissions
                        try:
                            os.makedirs(value)
                        except:
                            write_err1 = ('Cannot create ' + value
                                          + '. Check write permissions')
                            test_err(False, 'check_input', write_err1)                     
        else:
            if (use_default):
                value = default_val
                exist_warn = ('Key ' + str(key) + ' does not exist or has '
                              + 'value None in INI file. It will created ' 
                              + 'using the default value ' + str(default_val))
                if (test_warn(key in config.options(section), 'check_key', 
                    exist_warn)):
                    config.set(section, key, default_val)
            else:
                exist_err = ('Key ' + str(key) + ' does not exist or has '
                              + 'value None in INI file. '
                              + 'It MUST be included with a valid value. ')
                test_err(key in config.options(section), 'check_key', 
                         exist_err)
    except (ValueError, TypeError):
        reason = 'Input value cannot be parsed into correct data type.'
        condition = False
        replace_default(key, value, data_type, use_default, default_val, 
                        condition, reason)
            
    # Check if value is within accepted set of values
    if (acc_val):
        acc_list = acc_val.split()
        reason = 'Input value not in accepted list of values for key.'
        condition = bool(str(value) in acc_list)
        replace_default(key, value, data_type, use_default, default_val, 
                        condition, reason)
            
    # If int or float, test whether it is a positive value (if required)
    if(value and data_type in ['int', 'float'] and positive):
        pos_err = ('Key ' + str(key) + ' with input value ' 
                     + str(config.get(section, key)) + ' must be a POSITIVE ' 
                     'number of type ' + data_type)
        test_err(value >= 0, 'check_key', pos_err)
    
    return value
 

def check_input(section, path_ini, dt):
    """Function to load, check and correct if possible the contents of the
    INI file for the detectorDistortion script.
    When possible, it will use default values if input ones are absent 
    It will also ensure that directory paths are int the absolute format and
    create output directories when and if needed
    It can call warnings or errors, depending on how severe the issue is
    Returns a dictionary to be used in the rest of the script
    """
    
    # Create and load config object
    config = ConfigParser.ConfigParser()
    config.read(path_ini)

    error_msg = ('The provided INI file does not contain the ' + str(section) 
                 + ' section. All relevant user parameters must be present '
                 + 'in the INI file under that section.')
    test_err(section in config.sections(), 'check_input', error_msg)
    
    section_out = section + '_checked'
    
    if (section == 'detectorDistortion.config'):
    
        # Prepare list of [key, data_type, default_val, use_default, positive, acc_value]
        key_list = [['path_dark',       'path',    None,      True,   True,   None],
                    ['path_flat',       'path',    None,      True,   True,   None],
                    ['prefix_dark',     'str',     None,      True,   False,  None],
                    ['prefix_flat',     'str',     None,      True,   False,  None],
                    ['avg',             'str',     'average', True,   False,  'median average'],
                    ['prefix_files',    'str',     None,      True,   False,  None],
                    ['file_type',       'str',     None,      False,  False,  'edf tif tiff h5 hdf5'],
                    ['dir_out',         'path',    None,      False,  True,   None],
                    ['subdir_data',     'str',     'data',    True,   False,  None],
                    ['subdir_fig',      'str',     'figures', True,   False,  None],
                    ['roi_mode',        'str',     'full',    True,   False,  'auto full input'],
                    ['roi_default',     'roi',     None,      True,   False,  None],
                    ['under_sample',    'int',     1,         True,   True,   None],
                    ['motor_step',      'float',   None,      False,  True,   None],
                    ['fast_axis',       'int',     None,      False,  False,  '1 2'],
                    ['grid_resol',      'int',     1,         True,   True,   None],
                    ['corr_size',       'int',     10,        True,   True,   None],
                    ['inter_corr_step', 'int',     1,         True,   True,   None],
                    ['resol_first_pass','int',     35,        True,   True,   None],
                    ['half_width_fact', 'float',   2,         True,   True,   None],
                    ['mode',            'int',     2,         True,   False,  '0 1 2'],
                    ['integ_meth',      'int',     1,         True,   False,  '0 1 2'],
                    ['output_type',     'str',     'txt',     True,   False,  'txt mat npy']
                    ]
    
    elif (section == 'waveFront.config'):
        # Prepare list of [key, data_type, default_val, use_default, positive, acc_value]
        key_list = [['energy',          'float',   None,      False,  True,   None],
                    ['dist',            'float',   None,      False,  True,   None],
                    ['pix_size1',       'float',   1,         True,   True,   None],
                    ['pix_size2',       'float',   None,      True,   True,   None],
                    ['diff_wf',         'bool',    True,      True,   False,  None],
                    ['undistort',       'bool',    False,     True,   False,  None],
                    ['mask_half_size',  'int',     0,         True,   True,   None],
                    ['path_dark1',       'path',   None,      True,   True,   None],
                    ['path_flat1',       'path',   None,      True,   True,   None],
                    ['prefix_dark1',     'str',    None,      True,   False,  None],
                    ['prefix_flat1',     'str',    None,      True,   False,  None],
                    ['path_dark2',       'path',   None,      True,   True,   None],
                    ['path_flat2',       'path',   None,      True,   True,   None],
                    ['prefix_dark2',     'str',    None,      True,   False,  None],
                    ['prefix_flat2',     'str',    None,      True,   False,  None],
                    ['horz_disto_map1',  'path',   None,      True,   True,   None],
                    ['vert_disto_map1',  'path',   None,      True,   True,   None],
                    ['horz_disto_map2',  'path',   None,      True,   True,   None],
                    ['vert_disto_map2',  'path',   None,      True,   True,   None],
                    ['file_type',       'str',     None,      False,  False,  'edf tif tiff h5 hdf5'],
                    ['dir_out',         'path',    None,      False,  False,  None],
                    ['subdir_data',     'str',     'data',    True,   False,  None],
                    ['subdir_fig',      'str',     'figures', True,   False,  None],
                    ['roi_mode',        'str',     'full',    True,   False,  'auto full input'],
                    ['roi_default1',    'roi',     None,      True,   False,  None],
                    ['roi_default2',    'roi',     None,      True,   False,  None],
                    ['roi_default_dd1', 'roi',     None,      True,   False,  None],
                    ['roi_default_dd2', 'roi',     None,      True,   False,  None],
                    ['under_sample',    'int',     1,         True,   True,   None],
                    ['image_order',     'int',     1,         True,   False,  '1 2'],
                    ['corr_size',       'int',     10,        True,   True,   None],
                    ['grid_resol',      'int',     1,         True,   True,   None],
                    ['resol_first_pass','int',     35,        True,   True,   None],
                    ['half_width_fact', 'float',   2,         True,   True,   None],
                    ['mode',            'int',     2,         True,   False,  '0 1 2'],
                    ['output_type',     'str',     'txt',     True,   False,  'txt mat npy']
                    ]
    else:
        error_msg = 'Unknown section header: ' + str(section) + '.'
        test_err(False, 'check_input', error_msg)
        
    # Initialize parameter dictionary
    param = {}
    
    # Add new section to config object
    config.add_section(section_out)
    
    # Check parameters related to files and directories
    for [key, data_type, default_value, use_default, positive, acc_value] \
        in key_list:

        value = check_key(key, config, section, data_type, default_value, 
                          use_default, positive, acc_value)
                          
        config.set(section_out, key, str(value))
        
        param[key] = value
        
        # For the ROI_default parameter, convert string to array of integers
        # Already done in check_key
        if((data_type == 'roi' or data_type == 'mask') and value):
            param[key] = np.array(value)
            
    # Some adjustments depending on which script is running
     
    if (section == 'waveFront.config'):
        # For absolute or differential metorlogy:
        # When pix_size2 not set, use value of pix_size1
        if(param['pix_size2'] is None):
            param['pix_size2'] = param['pix_size1']
            config.set(section_out, 'pix_size2', str(param['pix_size2']))
        
        # For differential metrology, all keys for second detector receive
        # the values introduced for the keys for the first detector
        if(param['diff_wf']):
            for [key1, key2] in [['roi_default1', 'roi_default2'], 
                                 ['prefix_dark1', 'prefix_dark2'], 
                                 ['prefix_flat1', 'prefix_flat2'], 
                                 ['roi_default_dd1', 'roi_default_dd2']]:
                if(param[key2] is None):
                    param[key2] = param[key1]
                    config.set(section_out, key2, str(param[key2]))
                
    # Check output directory and subdirectories
    dir_out = param['dir_out']
    
    # Create write permissions for output directory
    write_err2 = ('Output directory ' + dir_out + 'does not have write'
                  ' permissions')
    test_err(os.access(dir_out, os.W_OK), 'check_input', write_err2)
        
    # Subdirectory for figures - create if does not exist
    if (not os.path.isdir(os.path.join(dir_out, 'figures'))):
            os.makedirs(os.path.join(dir_out, 'figures'))
    # Subdirectory for data files - create if does not exist
    if (not os.path.isdir(os.path.join(dir_out, 'data'))):
        os.makedirs(os.path.join(dir_out, 'data'))
                                      
    # Print a modified INI file in output directory (with new section)
    cfg_file = open(os.path.join(param['dir_out'], 'input_modified_' 
                    + dt + '.ini'), 'w')
    config.write(cfg_file)
    cfg_file.close()
    
    # Return dictionary containing needed parameters and their values
    return param


def check_input_dd(section, section_out, path_ini, dt):
    """Function to load, check and correct if possible the contents of the
    INI file for the detectorDistortion script.
    When possible, it will use default values if input ones are absent 
    It will also ensure that directory paths are int the absolute format and
    create output directories when and if needed
    It can call warnings or errors, depending on how severe the issue is
    Returns a dictionary to be used in the rest of the script
    """
    
    # Create and load config object
    config = ConfigParser.ConfigParser()
    config.read(path_ini)

    error_msg = ('The provided INI file does not contain the ' + str(section) 
                 + ' section. All relevant user parameters must be present '
                 + 'in the INI file under that section')
    test_err(section in config.sections(), 'check_input_dd', error_msg)
    
    # Prepare list of [key, data_type, default_val, use_default, positive, acc_value]
    key_list = [['path_dark',       'path',    None,      True,   False,  None],
                ['path_flat',       'path',    None,      True,   False,  None],
                ['prefix_dark',     'str',     None,      True,   False,  None],
                ['prefix_flat',     'str',     None,      True,   False,  None],
                ['avg',             'str',     'average', True,   False,  'median average'],
                ['prefix_files',    'str',     None,      True,   False,  None],
                ['file_type',       'str',     None,      False,  False,  'edf tif tiff h5 hdf5'],
                ['dir_out',         'path',    None,      False,  False,  None],
                ['roi_mode',        'str',     'full',    True,   False,  'auto full input'],
                ['roi_default',     'roi',     None,      True,   False,  None],
                ['under_sample',    'int',     1,         True,   True,   None],
                ['motor_step',      'float',   None,      False,  True,   None],
                ['fast_axis',       'int',     None,      False,  False,  '1 2'],
                ['grid_resol',      'int',     1,         True,   True,   None],
                ['corr_size',       'int',     10,        True,   True,   None],
                ['inter_corr_step', 'int',     1,         True,   True,   None],
                ['resol_first_pass','int',     35,        True,   True,   None],
                ['half_width_fact', 'float',   2,         True,   True,   None],
                ['mode',            'int',     2,         True,   False,  '0 1 2'],
                ['integ_meth',      'int',     1,         True,   False,  '0 1 2']
                ]
    
    # Initialize parameter dictionary
    param = {}
    
    # Add new section to config object
    config.add_section(section_out)
    
    # Check parameters related to files and directories
    for [key, data_type, default_value, use_default, positive, acc_value] in key_list:

        value = check_key(key, config, section, data_type, default_value, 
                          use_default, positive, acc_value)
                          
        config.set(section_out, key, str(value))
        
        param[key] = value
        
        # For the ROI_default parameter, convert string to array of integers
        if(data_type == 'roi' and value):
            roi = np.array([])
            for coor in value.split():
                roi = np.append(roi, int(coor))
            param[key] = roi
    
    # Create output directory and subdirectories
    dir_out = param['dir_out']
    
    # Create output directory if not created, check write permissions
    if (not os.path.isdir(dir_out)):
        try:
            os.makedirs(dir_out)
        except:
            write_err1 = ('Cannot create ' + dir_out + '. Check write '
                          ' permissions')
            test_err(False, 'check_input', write_err1)
    else:
        write_err2 = ('Output directory ' + dir_out + 'does not have write'
                      ' permissions')
        test_err(os.access(dir_out, os.W_OK), 'check_input', write_err2)
    # Subdirectory for figures
    if (not os.path.isdir(os.path.join(dir_out, 'figures'))):
            os.makedirs(os.path.join(dir_out, 'figures'))
    # Subdirectory for data files
    if (not os.path.isdir(os.path.join(dir_out, 'data'))):
        os.makedirs(os.path.join(dir_out, 'data'))

    #~ check_key('skip_every_n_files', config, section, 'int', default_val = -1) 
    #~ check_key('motor_v', config, section)
    #~ check_key('motor_h', config, section)   
    #~ check_key('my_lee_filter', config, section, 'int', default_val = 0)
    #~ check_key('filter_z', config, section, 'int', default_val = 0) 
                                      
    # Print a modified INI file in output directory (with new section)
    bn = os.path.basename(path_ini)
    cfg_file = open(os.path.join(param['dir_out'], 
                                'modified_' + dt +'_' + bn), 'w')
    config.write(cfg_file)
    cfg_file.close()
    
    # Return dictionary containing needed parameters and their values
    return param


def check_input_wf(section, section_out, path_ini, dt):
    """Function to check and correct if possible the contents of the
    input dictionary for the waveFront script
    When possible, it will use default values if input ones are absent 
    It will also ensure that directory paths end in '/'
    It can call warnings or errors, depending on how severe the issue is
    """
    
    # Check parameters related to experimental values and type of WF
    check_key('energy', config, section, 'float', use_default = False)
    check_key('dist', config, section, 'float', use_default = False)
    check_key('pix_size1', config, section, 'float', default_default = 1.0)
    check_key('pix_size2', config, section, 'float', default_val = config.get(section, 'pix_size1'))
    check_key('diff_wf', config, section, 'bool', default_val = False)
    check_key('undistort', config, section, 'bool', default_val = False)
    check_key('mask_half_size', config, section, 'int', default_val = 0)
    
    # Check parameters related to files and directories
    for key in ['path_dark1', 'path_dark2', 'path_flat1', 'path_flat2', 
            'horz_disto_map1', 'vert_disto_map1', 
            'horz_disto_map2', 'vert_disto_map2', 
            'dir_out']:
        if (key == 'dir_out'):
            check_key(key, config, section, use_default = False)
        else:
            check_key(key, config, section)
            
        if (config.get(section, key)):
            value = config.get(section, key)
            config.set(section, key, os.path.abspath(value))

    if (not os.path.isdir(config.get(section, 'dir_out'))):
        os.makedirs(config.get(section, 'dir_out'))

    if (not os.path.isdir(os.path.join(config.get(section, 'dir_out'), 'figures'))):
        os.makedirs(config.get(section, 'dir_out') + '/figures/')

    if (not os.path.isdir(os.path.join(config.get(section, 'dir_out'), 'wf_data'))):
        os.makedirs(config.get(section, 'dir_out') + '/wf_data/')
    
    # Check parameters related to filtering files
    check_key('prefix_dark1', config, section)
    check_key('prefix_dark2', config, section)
    check_key('prefix_flat1', config, section)
    check_key('prefix_flat2', config, section)
    check_key('file_type', config, section, use_default = False)

    # Check parameters related to reading in the images
    check_key('roi_mode', config, section, default_val = 'full')
    check_key('roi_default1', config, section, 'roi')
    check_key('roi_default2', config, section, 'roi')
    check_key('roi_default_dd1', config, section, 'roi')
    check_key('roi_default_dd2', config, section, 'roi')
    check_key('under_sample', config, section, 'int', default_val = 1)
    
    # Check parameters related to image filters
    
    # Check parameters related to cross-correlation
    check_key('image_order', config, section, 'int', default_val = 1)
    check_key('grid_resol', config, section, 'int', default_val = 4) 
    check_key('corr_size', config, section, 'int', default_val = 10) 
    check_key('half_width_fact', config, section, 'float', default_val = 2) 
    check_key('resol_first_pass', config, section, 'int', default_val = 35) 
    check_key('mode', config, section, 'int', default_val = 3)
    
    # Check parameters related to integration
    check_key('integ_meth', config, section, 'int', default_val = 1)
    
    
def check_input_xsvt(section, section_out, path_ini, dt):
    """Function to check and correct if possible the contents of the
    input dictionary for the XSVT script
    When possible, it will use default values if input ones are absent 
    It will also ensure that directory paths end in '/'
    It can call warnings or errors, depending on how severe the issue is
    """
    
    # Create and load config object
    config = ConfigParser.ConfigParser()
    config.read(path_ini)
    
    error_msg = ('The provided INI file does not contain the ' + str(section) 
                 + ' section. All relevant user parameters must be present '
                 + 'in the INI file under that section')
    test_err(section in config.sections(), 'check_input_xsvt', error_msg)
    
    # Prepare list of [key, data_type, default_val, use_default, positive, acc_value]
    key_list = [['energy',          'float',   None,      False,  True,   None],
                ['delta',           'float',   None,      False,  True,   None],
                ['dist',            'float',   None,      False,  True,   None],
                ['pix_size',        'float',   1,         True,   True,   None],
                ['num_lens',        'float',   None,      False,  True,   None],
                ['dir_sample',      'path',    None,      False,  False,  None],
                ['dir_ref',         'path',    None,      False,  False,  None],
                ['diff_wf',         'bool',    True,      True,   False,  None],
                ['undistort',       'bool',    False,     True,   False,  None],
                ['mask_half_size',  'int',     0,         True,   True,   None],
                ['path_dark',       'path',    None,      True,   False,  None],
                ['path_flat',       'path',    None,      True,   False,  None],
                ['prefix_dark',     'str',     None,      True,   False,  None],
                ['prefix_flat',     'str',     None,      True,   False,  None],
                ['horz_disto_map',  'path',    None,      True,   False,  None],
                ['vert_disto_map',  'path',    None,      True,   False,  None],
                ['prefix_sample',   'str',     None,      True,   False,  None],
                ['prefix_ref',      'str',     None,      True,   False,  None],
                ['file_type',       'str',     None,      False,  False,  'edf tif tiff h5 hdf5'],
                ['dir_out',         'path',    None,      False,  False,  None],
                ['out_str',         'str',     None,      True,   False,  None],
                ['roi_mode',        'str',     'full',    True,   False,  'auto full input'],
                ['roi_default',     'roi',     None,      True,   False,  None],
                ['roi_default_dd',  'roi',     None,      True,   False,  None],
                ['file_skip_step',  'int',     1,         True,   True,   None],
                ['under_sample',    'int',     1,         True,   True,   None],
                ['image_order',     'int',     1,         True,   False,  '1 2'],
                ['corr_size',       'int',     10,        True,   True,   None],
                ['grid_resol',      'int',     1,         True,   True,   None],
                ['resol_first_pass','int',     35,        True,   True,   None],
                ['half_width_fact', 'float',   2,         True,   True,   None],
                ['mode',            'int',     2,         True,   False,  '0 1 2'],
                ['integ_meth',      'int',     1,         True,   False,  '0 1 2']
                ]
    
    # Initialize parameter dictionary
    param = {}
    
    # Add new section to config object
    config.add_section(section_out)
    
    # Check parameters related to files and directories
    for [key, data_type, default_value, use_default, positive, acc_value] in key_list:

        value = check_key(key, config, section, data_type, default_value, 
                          use_default, positive, acc_value)
                          
        config.set(section_out, key, str(value))
        
        param[key] = value
        
        # For the ROI_default parameter, convert string to array of integers
        if(data_type == 'roi' and value):
            roi = np.array([])
            for coor in value.split():
                roi = np.append(roi, int(coor))
            param[key] = roi
    
    # Create output directory and subdirectories
    dir_out = param['dir_out']
    
    # Create output directory if not created, check write permissions
    if (not os.path.isdir(dir_out)):
        try:
            os.makedirs(dir_out)
        except:
            write_err1 = ('Cannot create ' + dir_out + '. Check write '
                          ' permissions')
            test_err(False, 'check_input', write_err1)
    else:
        write_err2 = ('Output directory ' + dir_out + 'does not have write'
                      ' permissions')
        test_err(os.access(dir_out, os.W_OK), 'check_input', write_err2)
    # Subdirectory for figures
    if (not os.path.isdir(os.path.join(dir_out, 'figures'))):
            os.makedirs(os.path.join(dir_out, 'figures'))
    # Subdirectory for data files
    if (not os.path.isdir(os.path.join(dir_out, 'data'))):
        os.makedirs(os.path.join(dir_out, 'data'))
                         
    # Print a modified INI file in output directory (with new section)
    cfg_file = open(os.path.join(param['dir_out'], 'input_modified_' + dt + '.ini'), 'w')
    config.write(cfg_file)
    cfg_file.close()
    
    # Return dictionary containing needed parameters and their values
    return param


def drange(x, y, jump):
  while x < y:
    yield float(x)
    x += decimal.Decimal(jump)


def max2(matrix):
    """Short function to replicate MATLAB max2 (return max and its 
    position)
    """
    
    max_val = np.amax(matrix)
    max_idx = np.unravel_index(np.argmax(matrix),matrix.shape)
    
    return max_val, max_idx


def extrap1d(interpolator, axis):
    """ Wrapper around the 1D interpolation function which takes care 
    of linear extrapolation. Useful in case of older versions of scipy 
    where interp1d does not come with the extrapolation option.
    Source: goo.gl/NWXgGJ (author: sastanin)
    Modified to support 2 dimensions for y (axis 0 or 1)
    """
    
    err_msg = ('The parameter axis can only hold the values 0 or 1. '
               + 'In present call: axis = ' + str(axis))
    test_err(bool(axis is 0 or axis is 1), 'extrap1d', err_msg)
    
    xs = interpolator.x
    ys = interpolator.y

    def point_wise(x):
        if (axis is 0):
            if x < xs[0]:
                    return (ys[0, :] + (x - xs[0]) * (ys[1, :] - ys[0, :])
                           / (xs[1] - xs[0]))
            elif x > xs[-1]:
                return (ys[-1, :] + (x - xs[-1]) * (ys[-1, :] - ys[-2, :])
                       / (xs[-1] - xs[-2]))
            else:
                return interpolator(x)
        elif (axis is 1):
            if x < xs[0]:
                return (ys[:, 0] + (x - xs[0]) * (ys[:, 1] - ys[:, 0])
                       / (xs[1] - xs[0]))
            elif x > xs[-1]:
                return (ys[:, -1] + (x - xs[-1]) * (ys[:, -1] - ys[:, -2])
                       / (xs[-1] - xs[-2]))
            else:
                return interpolator(x)

    def u_func_like(xs):
        return scipy.array(map(point_wise, scipy.array(xs)))

    return u_func_like


def motor_pos(header, mesh, i, k, motor_v, motor_h, motor_a):
    motor_vert_pos = header['motor_pos'].split()[header['motor_mne'].split()
                   .index(motor_v)]
    motor_horz_pos = header['motor_pos'].split()[header['motor_mne'].split()
                   .index(motor_h)]
    motor_axial_pos = header['motor_pos'].split()[header['motor_mne'].split()
                   .index(motor_a)]
    # mesh = [ydet1, xdet1, zdet1, ydet2, xdet2, zdet2]
    mesh[k, i * 3 + 0] = motor_vert_pos
    mesh[k, i * 3 + 1] = motor_horz_pos
    mesh[k, i * 3 + 2] = motor_axial_pos
    
    return mesh
    
    
def setup_motor_mesh(direction, fast_axis, inter_corr_step, n_images):

    rtn_images = int(math.sqrt(n_images))

    # Direction coincides with FAST AXIS
    if (direction == fast_axis):
        inter_corr_step0 = inter_corr_step
        ksel = range(0, n_images)
        ksel = [x for x in ksel if (x+1) % rtn_images != 0]
        #~ ksel = filter(lambda x: (x+1) % rtn_images != 0, ksel)
        for i in range(1, inter_corr_step0):
            ksel = [x for ksel in n if (x+1) % rtn_images != (rtn_images - i)]
            #~ ksel = filter(lambda x: (x+1) % rtn_images != (rtn_images - i), ksel)
            
    # Direction coincides with SLOW AXIS
    else:
        inter_corr_step0 = inter_corr_step
        inter_corr_step0 = inter_corr_step0 * rtn_images
        ksel = range(0, n_images - inter_corr_step0)

    return [ksel, inter_corr_step0]

 
def compute_corr_field(path_corr, prefix_files, file_type, string, 
                       meth = 'average'):
    """Function for obtaining average or median Dark or Flat field 
    correction image based on input parameters: 
    - path to locate image or folder containing several images
    - prefix for images - if given, if not leave blank ('') or with 
    value 'none'
    - suffix for images - defines file type, should be known and 
    provided
    - type of average to use (in case of several images, if must be 
    supplied 
    - and it must have the values 'average' or 'median')
    
    It returns Dark or Flat Field image.
    """
    
    # Do we have needed Corrrection Field file?
    have_corr = 0
    image_corr_avg = 100

    if (not path_corr):
        image_corr = ''
        logging.info('No file for ' + str(string) + ' Field, will not perform ' 
                    'this correction.')
    else:
        # Did the user give us only one file for the Corrrection Field?
        if (path_corr[-len(file_type):].lower() == file_type.lower()):
            have_corr = 1
            if (file_type.lower() == 'edf'):
                image_corr = EdfFile(path_corr).GetData(0).astype('float32')      
            elif (file_type.lower() == 'tif' or 
                  file_type.lower() == 'tiff'):
                image_corr = np.array(Image.open(path_corr)).astype('float32') 
            logging.info('Using file ' + path_corr + ' for ' + str(string) 
                        + ' Field correction.')
        
        # Did the user give us a folder for Corrrection Field images?  
        if (have_corr is 0):
            have_corr = 2
            
            logging.info('Using folder ' + path_corr + ' for ' + str(string) 
                        + ' Field correction.')
            
            file_list_corr = []
            for (dir_path, dir_names, file_names) in walk(path_corr):
                file_list_corr.extend(file_names)
                break
                            
            #Check if this list is empty. If yes, call error
            
            empty_err = ('The list of files for ' + str(string) + ' Field '
                         + 'correction from folder ' + str(path_corr) 
                         + ' is EMPTY. Double-check the path.')
            test_err(bool(len(file_list_corr)), 'compute_corr_field', 
                     empty_err)

            # Filter files in folder starting with prefix if given
            if (prefix_files):
                file_list_corr = [x for x in file_list_corr if x.startswith(prefix_files)]
                #~ file_list_corr = filter(lambda x: x.startswith(prefix_files), 
                                      #~ file_list_corr)
                
                empty_err = ('The list of files for ' + str(string) + ' Field '
                             + 'correction from folder ' + str(path_corr) 
                             + ' after filtering by prefix ' 
                             + str(prefix_files) 
                             + ' is EMPTY. Double-check the path and prefix.')
                test_err(bool(len(file_list_corr)), 'compute_corr_field', 
                         empty_err)
                         
                logging.info(str(len(file_list_corr)) + ' correction files')
                
            if (meth is 'median'):
                stack_corr = []
                
                for file_name in file_list_corr:
                    
                    if (file_type.lower() == 'edf'):
                        image_corr = (EdfFile(os.path.join(path_corr, 
                                      file_name)).GetData(0)
                                      .astype('float32'))
                    elif (file_type.lower() == 'tif' or 
                          file_type.lower() == 'tiff'):
                        image_corr = (np.array(Image.open(os.path.join(path_corr, 
                                    file_name))).astype('float32'))
                    else:
                        logging.info('Unknown suffix for correction '
                                    'field file.')
    
                    stack_corr.append(image_corr)
                
                image_corr = np.median(stack_corr, axis = 0)
                
            elif (meth is 'average'):
                
                if (file_type.lower() == 'edf'):
                    array_shape = (EdfFile(os.path.join(path_corr, 
                                    file_list_corr[0])).GetData(0)
                                    .astype('float32').shape) 
                elif (file_type.lower() == 'tif' or 
                      file_type.lower() == 'tiff'):
                    array_shape = (np.array(Image.open(os.path.join(path_corr, 
                                   file_list_corr[0]))).astype('float32')
                                   .shape)
                else:
                    logging.info('Unknown suffix for correction field file.')
                
                logging.info('File type ' + file_type.lower())
                
                mean_corr = np.zeros(array_shape)
                count = 0
                for file_name in file_list_corr:
                    count = count + 1
                    
                    if (file_type.lower() == 'edf'):
                        image_corr = (EdfFile(os.path.join(path_corr, file_name)).GetData(0)
                                      .astype('float32'))
                    elif (file_type.lower() == 'tif' or 
                          file_type.lower() == 'tiff'):
                          image_corr = (np.array(Image.open(path_corr 
                                        + file_name)).astype('float32'))
                        
                    mean_corr = mean_corr + image_corr
                    
                image_corr = mean_corr / float(count)
            
            else:        
                test_err(False, 'compute_corr_field', 'Input value for '
                         + 'parameter method has to be "average" or "median". '
                         + 'Supplied value: "' + str(avg))
            
            image_corr = image_corr.astype('float32')
            image_corr_avg = np.average(image_corr)
            
    return [image_corr, have_corr, image_corr_avg]


def correction(image, have_flat, flat_img, have_dark, dark_img):
    """Function for doing dark&flat field corrections to image
    Returns corrected image
    """
    
    # After removing dark field, force all negative values to 0
    # For numerator, force all null and negative values to 0.00000001
    if (have_flat != 0 and have_dark != 0):
        numerator = (flat_img - dark_img).clip(0)
        numerator[numerator == 0] = 0.00000001
        image = (image - dark_img) / numerator
    elif (have_dark != 0):
        image = (image - dark_img).clip(0)
    elif (have_flat != 0):
        flat_img[flat_img == 0] = 0.00000001
        image = image / flat_img
                
    return image
    
    
def roi_auto(image0, dark_avg=100, fact=1.3, edgeOffset=25):
    """Function to automatically compute the Region Of Interest (ROI) 
    of an image. It returns the the computed ROI
    It INCLUDES borders and consideres that the roi values are for an
    indexing starting with 1, not 0, like in py indexing.
    """
    [n, m] = image0.shape
    
    roi = np.array([1, n, 1, m])
    
    for i in range(0, n):
        if (np.average(image0[i, :]) > fact * dark_avg):
            roi[0] = i + 1
            break
        else:
            pass
            
    for i in reversed(range(0,n)):
        if (np.average(image0[i, :]) > fact * dark_avg):
            roi[1] = i + 1
            break
        else:
            pass
            
    for i in range(0, m):
        if (np.average(image0[:, i]) > fact * dark_avg):
            roi[2] = i + 1
            break
        else:
            pass
            
    for i in reversed(range(0, m)):
        if (np.average(image0[:, i]) > fact * dark_avg):
            roi[3] = i + 1
            break
        else:
            pass
                
    return roi


def roi_detect(image0, roi_mode, roi_input=0, dark_avg=100, fact=1.3, 
               edge_offset=25, correct_offset=False):
    """Function to apply Region Of Interest (roi) to an image
    It return an image corresponding to the input roi
    It INCLUDES borders and consideres that the roi values are for an
    indexing starting with 1, not 0, like in py indexing.
    """

    [n, m] = image0.shape
    
    roi = np.array([1, n, 1, m])
    
    if (roi_mode == 'full' or (roi_mode == 'none' and np.all(roi_input == 0))):
        pass
    elif (roi_mode == 'auto'):
        for i in range(0, n):
            if (np.average(image0[i, :]) > fact * dark_avg):
                roi[0] = i + 1
                break
            else:
                pass
                
        for i in reversed(range(0,n)):
            if (np.average(image0[i, :]) > fact * dark_avg):
                roi[1] = i + 1
                break
            else:
                pass
                
        for i in range(0, m):
            if (np.average(image0[:, i]) > fact * dark_avg):
                roi[2] = i + 1
                break
            else:
                pass
                
        for i in reversed(range(0, m)):
            if (np.average(image0[:, i]) > fact * dark_avg):
                roi[3] = i + 1
                break
            else:
                pass
                
    else:
        roi = roi_input
        
    # Necessary for Shimadzu data
    if(correct_offset):
        if (roi[0] <= edge_offset):
            roi[0] = edgeOffset

        if (roi[1] >= n - edge_offset):
            roi[1] = n - edge_offset       
            
        if (roi[2] <= edge_offset):
            roi[2] = edge_offset
            
        if (roi[3] >= m - edge_offset):
            roi[3] = m - edge_offset
                
    return roi


def roi_select(image0, roi_input = None):
    """Function to apply Region Of Interest (ROI) to an image
    It return an image corresponding to the input ROI. If there is not ROI, it
    just return the full image.
    It INCLUDES borders and consideres that the roi values are for an
    indexing starting with 1, not 0, like in py indexing.
    """
    
    if(roi_input is None):
        image = image0
    else:
        roi_err = ('ROI: ' + str(roi_input) + ' is out of bounds '
                   + 'for image of size: ' + str(image0.shape))

        test_err(roi_input[0] > 0 and roi_input[1] <= image0.shape[0] 
                 and roi_input[2] > 0 and roi_input[3] <= image0.shape[1], 
                 'roi_select', roi_err)
            
        image = image0[int(roi_input[0] - 1) : int(roi_input[1]), 
                       int(roi_input[2] - 1) : int(roi_input[3])]
        
    return image


def roi_recalc(roi, roi_dd):
    """Function to recalculate Region Of Interest (roi) of an image
    after undistorting, using the roi used for computing the distortion maps
    It INCLUDES borders and consideres that the roi values are for an
    indexing starting with 1, not 0, like in py indexing.
    """
    
    roi_new = roi
    
    # CHECK THAT ROI is numpy array or a list that can be converted to array
    # use isinstance
    # CHECK THAT it has 4 elements

    if(isinstance(roi, np.ndarray) and isinstance(roi_dd, np.ndarray)):
    
        roi_new = np.array([1, int(roi_dd[1] - roi_dd[0] + 1), 
                            1, int(roi_dd[3] - roi_dd[2] + 1)])
        
        no_overlap_err = ('roi ' + str(roi) + ' and roi_dd ' + str(roi_dd)
                          + ' do not overlap')
                          
        test_err(roi[1] >= roi_dd[0] and roi[3] >= roi_dd[2]
                 and roi_dd[1] >= roi[0] and roi_dd[3] >= roi[2], 
                 'roi_recalc', no_overlap_err)

        if (roi[0] > roi_dd[0]):
            roi_new[0] = int(roi[0] - roi_dd[0] + 1)
        if (roi[1] < roi_dd[1]):
            roi_new[1] = int(roi[1] - roi_dd[0] + 1)
        if (roi[2] > roi_dd[2]):
            roi_new[2] = int(roi[2] - roi_dd[2] + 1)
        if (roi[3] < roi_dd[3]):
            roi_new[3] = int(roi[3] - roi_dd[2] + 1)
        
    return roi_new

 
def my_erosion(image, val_thresh, filt_sz=5):
    """ Function to apply erosion filter in order to remove some of the 
    impulse noise 
    It returns filtered image 
    val_thresh = the value above which the pixel value must be changed
    varMargin = optional field, an integer value for the size of the 
    median filter, default value is 5

    Based on a Matlab function by S. Berujon
    """

    # Input image size
    [m, n] = image.shape
    
    # GIVEN THAT MIRROR IS USED, CONSIDER TO REMOVE EXTRA CORNER TREATMENT!
    # Indices for 'corners' of input image
    pts2 = ((0, 0, 0, 0, 1, 1, m-2, m-2, m-1, m-1, m-1, m-1 ), 
            (0, 1, n-2, n-1, 0, n-1, 0, n-1, 0, 1, n-2 , n-1))
    # Values in these 'corners'
    val2 = image[pts2]
    # Apply 2D median filter - using option mode 'mirror' equivalent to 
    # 'symmetric' in Matlab
    image_filt = scipy.ndimage.filters.median_filter(image,filt_sz, 
                                                     mode='mirror')
    # Check difference between initial and filtered images element by element    
    diff_img = abs(image - image_filt)
    # Consider this difference null for 'corners'
    diff_img[pts2] = 0
    # Check which elements have a difference above the treshold value
    pts1 = np.where(diff_img > val_thresh)
    # Replace these elements with median value, excluding 'corners'    
    image[pts1] = image_filt[pts1]
    
    return image

       
def avg_filter(image, filt_sz):
    """Function to reproduce MATLAB imfilter with a filterz sized 
    averaging kernel
    It returns the filtered image
    Input: the image to be filtered and the an integer showing the 
    kernel size (filt_sz x filt_sz)
    It uses mode 'nearest' equivalent to MATLAB 'replicate' 
    """
    
    kernel = np.ones([filt_sz, filt_sz]) / float(filt_sz * filt_sz)    
    output = scipy.ndimage.correlate(image, kernel, mode = 'nearest')
    
    return output

   
def f_gaussian(size=[3, 3], sigma=0.5):
    """Source: 
    https://mail.scipy.org/pipermail/scipy-user/2013-May/034563.html
    
    Function to reproduce MATLAB fspecial('gaussian', hsize, sigma), 
    that returns a rotationally symmetric Gaussian lowpass filter of 
    size hsize with standard deviation sigma (positive). hsize can be a 
    vector specifying the number of rows and columns in output. 

    As in Matlab, the default value for hsize is [3 3]; the default 
    value for sigma is 0.5.
     
    (MATLAB says: Not recommended. Use imgaussfilt or imgaussfilt3 
    instead.)
    
    It returns the 2D filter
    """
    
    m, n = size
    h, k = m//2, n//2
    x, y = np.mgrid[-h : h, -k : k]
    
    return np.exp(-(x**2 + y**2) / (2 * sigma**2))

   
def crop_rect(img, rect):
    """Function that crops the input image img according to the 
    four-element array
    
    rect = [xmin ymin width height]. It returns the cropped image.
    !Width and height now count all pixels in cropped rectangle
    
    This is a Python version for MATLAB imcrop subroutine.
    ! Modified August 2018, removed + 1 from 2nd and 4th elements
    ! Double check in Matlab
    """

    img2 = img[int(rect[1]) : int(rect[1] + rect[3]), 
               int(rect[0]) : int(rect[0] + rect[2])]

    return img2

   
def cp_corr(corr_size, half_width_fact, xy_input_in, xy_base_in, input, base):
    """Function to tune control point locations using cross-correlation.

    INPUT_POINTS = CPCORR(INPUT_POINTS_IN,BASE_POINTS_IN,INPUT,BASE) 
    uses     normalized cross-correlation to adjust each pair of control 
    points     specified in INPUT_POINTS_IN and BASE_POINTS_IN.

    INPUT_POINTS_IN must be an M-by-2 double matrix containing the
    coordinates of control points in the input image.  BASE_POINTS_IN is
    an M-by-2 double matrix containing the coordinates of control points
    in the base image.

    CPCORR returns the adjusted control points in INPUT_POINTS, a double
    matrix the same size as INPUT_POINTS_IN.  If CPCORR cannot correlate 
    a pair of control points, INPUT_POINTS will contain the same 
    coordinates as INPUT_POINTS_IN for that pair.

    CPCORR will only move the position of a control point by up to 4
    pixels.  Adjusted coordinates are accurate up to one tenth of a
    pixel.  CPCORR is designed to get subpixel accuracy from the image
    content and coarse control point selection.

    Note that the INPUT and BASE images must have the same scale for
    CPCORR to be effective.

    CPCORR cannot adjust a point if any of the following occur:
    - points are too near the edge of either image
    - regions of images around points contain Inf or NaN
    - region around a point in input image has zero standard deviation
    - regions of images around points are poorly correlated

    The images can be numeric and must contain finite values. The input
    control point pairs are double.

    Python version based on MATLAB cpcorr subroutine
    """
         
    # Get all rectangle coordinates
    rects_input = calc_rects(xy_input_in, corr_size, input.shape)

    rects_base = calc_rects(xy_base_in, half_width_fact * corr_size, base.shape)
    
    ncp = xy_input_in.shape[0]
    
    # Initialize adjusted control points matrix
    xy_input = np.copy(xy_input_in)
    threshold = 0.3
    
    for icp in range(ncp):
        
        #~ if (np.any(rects_input[icp, 2 : 3] == 0) or 
            #~ np.any(rects_base[icp, 2 : 3] == 0)): 
        if ((rects_input[icp, 2 : 3] is [0, 0]) or 
            (rects_base[icp, 2 : 3] is [0, 0])):
            #~ print(rects_input[icp, 2 : 3])
            #~ print(rects_base[icp, 2 : 3])
            # Near edge, unable to adjust
            continue
        
        sub_input = crop_rect(input,rects_input[icp,:])
        sub_base = crop_rect(base,rects_base[icp,:])
        
        inputSize = sub_input.shape
        
        if (0 in inputSize):
            # 0 size input template
            continue
                    
        if (np.any(np.logical_not(np.isfinite(sub_input))) or 
           (np.any(np.logical_not(np.isfinite(sub_base))))):
            # NaN or Inf, unable to adjust
            continue
        
        # Check that the template rectangle sub_input has non-zero std
        if (np.std(sub_input) == 0.0):
            # If zero standard deviation of template image, unable to 
            # adjust
            continue
            
        if (sub_input.shape == (0, 0) or sub_base.shape == (0, 0)):
            #~ print(icp)
            #~ print([sub_input.shape, sub_base.shape])
            #~ print([rects_input[icp,:], rects_base[icp,:]])
            continue #exit()
            
        corr_map, min_val, max_val, min_loc, max_loc = nx.norm_xcorr(sub_input,
                                                                     sub_base)
        
        pic = nx.nxcorr_disp(corr_map)
        
        amplitude = max_val
        
        if (amplitude < threshold): 
            # Low correlation, unable to adjust
            continue
        
        # Offset found by cross correlation (xOffset, yOffset)
        corr_offset = [pic[1], pic[0]]

        # Eliminate any big changes in control points' positions
        ind = np.where(np.abs(corr_offset) > (corr_size - 1))
                
        ind = ind[0]
        
        # Using len to test for an empty array is better than checking 
        # the truth value of an empty array because the array can contain 
        # position 0, which reads False, althought the array is not 
        # empty in that case
        if (len(ind)):
            # Peak of norm_xcorr not well constrained, unable to adjust
            continue
            
        input_frac_offset = (xy_input[icp, :] 
                             - np.round(xy_input[icp, :] * 100) 
                             / 100)
        base_frac_offset = (xy_base_in[icp, :] 
                            - np.round(xy_base_in[icp, :] * 100) 
                            / 100)
        
        # Adjust control point        
        xy_input[icp, :] = (xy_input[icp, :] - input_frac_offset - corr_offset 
                           + base_frac_offset)
        
    
    return xy_input
   
   
def calc_rects(xy, half_width, img_shape):
    """Function to calculate rectangles so that imcrop function will 
    return the image with xy coordinate inside the central pixel
    """
    
    default_width = 2 * half_width
    default_height = default_width

    # xy specifies center of rectangle, need upper left
    upperLeft = np.round(xy) - half_width

    # Need to modify for pixels near edge of images
    upper = upperLeft[:, 1]
    left = upperLeft[:, 0]
    lower = upper + default_height
    right = left + default_width
    width = default_width * np.ones(upper.shape)
    height = default_height * np.ones(upper.shape)
    
    # Check edges for coordinates outside image
    [upper, height] = adjust_edge(upper, 0, height, 1)
    [tmp, height] = adjust_edge(lower, img_shape[0]-1, height, 2)
    [left, width] = adjust_edge(left, 0, width, 1)
    [tmp, width] = adjust_edge(right, img_shape[1]-1, width, 2)
    
    # Set width and height to zero when less than the default size
    iw = np.where(width < default_width)
    ih = np.where(height < default_height)
    idx = np.unique(np.concatenate(iw + ih, axis=0))
    width[idx] = 0
    height[idx] = 0

    rect = np.column_stack((left, upper, width, height))

    return rect

  
def adjust_edge(coordinates, edge_margin, breadth, param):
    """Function to adjust coordinates when the edges surpass the 
    margin
    For lower edge, param = 1
    For upper (higher) edge, param = 2
    breadth refers to wideness, which must also be adjusted if needed
    """
    
    # WHAT IF coordinates are NEGATIVE?
    # WHAT IF breaths are NEGATIVE?
    
    test_err(param is 1 or param is 2, 'adjust_edge', 'param must be 1 or 2')
    
    # For lower edges check for those below the margin 
    if (param is 1): 
        indx = np.where(coordinates < edge_margin)
    # For upper edges check for those above the margin 
    elif (param is 2): 
        indx = np.where(coordinates > edge_margin)
    
    # In case we have something to correct
    if (len(indx)):
        # Substract difference between edge and margin form breath
        breadth[indx] = breadth[indx] - abs(coordinates[indx] - edge_margin)
        # Force coordinate to be equal to margin
        coordinates[indx] = edge_margin

    return [coordinates, breadth]


def plane_fit(speck_dis, mask = None):
    """ (New) function to fit a surface to a plane using a mask
    Plane equation: ax + by + c = z
    Eq. to solve: Ax = B, where 
    A = [[x0 y0 1], [x1, y1, 1], ..., [xn, yn, 1]]
    x = [a, b, c]
    B = [z0, z1, ... zn]
    Solution: [a, b, c] = (A^T * A)^(-1) * A^T * B
    a = slope in x direction, b = slope in y direction
     
    Source: https://stackoverflow.com/questions/35005386/fitting-a-plane-to-a-2d-array
    Mathematical explanation: https://math.stackexchange.com/questions/99299/best-fitting-plane-given-a-set-of-points
    
    """
    
    # The type has to be bool here, NOT int
    if (mask is None):
        mask = np.ones(speck_dis.shape).astype(bool)
    else:
        mask = mask.astype(bool)

    # Size of input surface
    [n, m] = speck_dis.shape

    [X1, Y1] = np.meshgrid(range(m), range(n))

    #Regression 
    XY0 = np.hstack((np.reshape(X1, (-1, 1)), np.reshape(Y1, (-1, 1))))
    XY0 = np.hstack((XY0, np.reshape(np.ones((n, m)), (-1, 1))))
    XY = np.hstack((np.reshape(X1[mask], (-1, 1)), np.reshape(Y1[mask], (-1, 1))))
    XY = np.hstack((XY, np.reshape(np.ones((n, m))[mask], (-1, 1))))
    ZZ = np.reshape(speck_dis[mask], (-1, 1))
        
    theta = np.dot(np.dot( np.linalg.pinv(np.dot(XY.transpose(), XY)), XY.transpose()), ZZ)
    
    slope_x = theta[0]
    slope_y = theta[1]

    plane = np.reshape(np.dot(XY0, theta), (n, m))

    return [plane, slope_x, slope_y]

    
def first_pass(row, col, img1, img2, roi1, roi2, szs):
    """Function to follow the displacement of the speckles from two 
    images in an initial, rough, sweep
    Uses external norm_xcorr subroutine

    Based on a Matlab function by S. Berujon
    """
    
    # For different rois you need to compute the offset vector even in 
    # diferential. mode
    
    # Size of search image
    [m1, n1] = img1.shape
    [m2, n2] = img2.shape
    
    # Define small area to select (target subset)
    if(isinstance(roi1, np.ndarray)):
        c1 = ((roi1[0] - 1) + row) - szs.st_size
        c2 = ((roi1[0] - 1) + row) + szs.st_size
        
        d1 = ((roi1[2] - 1) + col) - szs.st_size
        d2 = ((roi1[2] - 1) + col) + szs.st_size
    else:
        c1 = row - szs.st_size
        c2 = row + szs.st_size
        
        d1 = col - szs.st_size
        d2 = col + szs.st_size
        
    # Check borders for target subset
    if (c1 < 0):
        c1 = 0
        c2 = c1 + 2 * szs.st_size + 1
        
    #if (c2 >= m2):
    if (c2 >= m1):
        #c2 = m2 - 1
        c2 = m1 - 1
        c1 = c2 - 2 * szs.st_size + 1
        
    if (d1 < 0):
        d1 = 0
        d2 = d1 + 2 * szs.st_size + 1
    
    #if (d2 >= n2):
    if (d2 >= n1):
        #d2 = n2 - 1
        d2 = n1 - 1
        d1 = d2 - 2 * szs.st_size + 1
            
    zone1 = img1[int(c1) : int(c2 + 1), int(d1) : int(d2 + 1)]
    
    # Define search area in 2nd image around target subset + offset
    if(isinstance(roi2, np.ndarray)):
        a1 = ((roi2[0]-1) + row) - szs.sa_size_v + szs.sa_offset_v
        a2 = ((roi2[0]-1) + row) + szs.sa_size_v + szs.sa_offset_v
        
        b1 = ((roi2[2]-1) + col) - szs.sa_size_h + szs.sa_offset_h
        b2 = ((roi2[2]-1) + col) + szs.sa_size_h + szs.sa_offset_h
    else:
        a1 = row - szs.sa_size_v + szs.sa_offset_v
        a2 = row + szs.sa_size_v + szs.sa_offset_v
        
        b1 = col - szs.sa_size_h + szs.sa_offset_h
        b2 = col + szs.sa_size_h + szs.sa_offset_h

    
    xf = 0
    yf = 0
        
    # Check borders for search area
    if (a1 < 0):
        yf = -a1
        a1 = 0 
        a2 = a2 + yf
        
    if (a2 >= m2):
        a2 = m2 - 1
        
    if (b1 < 0):
        xf = -b1
        b1 = 0
        b2 = b2 + xf
        
    if (b2 >= n2):
        b2 = n2 - 1
    
    if (((a2 - a1) < zone1.shape[0]) and (a2 == m2-1)):
        #yf = a1 - (a2 - szs.sa_size_v)
        #a1 = a2 - szs.sa_size_v
        yf = a1 - (a2 - 2 * szs.sa_size_v + 1)
        a1 = a2 - 2 * szs.sa_size_v + 1
    
    if (((b2 - b1) < zone1.shape[1]) and (b2 == n2-1)):
        #xf = b1 - (b2 - szs.sa_size_h)
        #b1 = b2 - szs.sa_size_h
        xf = b1 - (b2 - 2 * szs.sa_size_h + 1)
        b1 = b2 - 2 * szs.sa_size_h + 1
        
    zone2 = img2[int(a1) : int(a2 + 1), int(b1) : int(b2 + 1)]
    
    max_val = DEFAULT
    peak_to_x = DEFAULT
    peak_to_y = DEFAULT

    if (zone1.size and zone2.size):
        corr, min_val, max_val, min_loc, max_loc = nx.norm_xcorr(zone1, zone2)

        peak_to_y = (max_loc[1] - (szs.sa_size_v - szs.st_size) + yf 
                     + szs.sa_offset_v)
        peak_to_x = (max_loc[0] - (szs.sa_size_h - szs.st_size) + xf 
                     + szs.sa_offset_h)

    else:
        test_err(False, 'first_pass', 'First pass unsuccessful.')
        
    return peak_to_y, peak_to_x, max_val

        
def shift_bit_length(x):
    return 1<<(x-1).bit_length()

        
def padpad(data, iterations=1):
    narray = data
    for i in xrange(iterations):
        length = len(narray)
        diff = shift_bit_length(length + 1) - length
        if length % 2 is 0:
            pad_width = diff / 2
        else:
            # need an uneven padding for odd-number lengths
            left_pad = diff / 2
            right_pad = diff - left_pad
            pad_width = (left_pad, right_pad)
        narray = np.pad(narray, pad_width, 'constant')
    return narray


class SearchObj:
    def __init__(self):
        # Constructor
        # st = search template
        # sa = search area
        self.st_size = 0
        self.sa_size_v = 0
        self.sa_size_h = 0
        self.sa_offset_v = 0
        self.sa_offset_h = 0

    
def cross_spot(img1, img2, roi_mode, roi1, roi2, grid_resol, mode, corr_size, 
               resol, half_width_fact, under_sample, dir_out, file_name1, 
               file_name2):
    """Function to follow the displacement of the speckles from two 
    images
    
    Uses external norm_xcorr subroutine
    
    Input:
      the two images
      the two roi
      grid_resol
      mode = 1 (small variations), 
             2 (rigid translation), 
             3 (no assumptions)
      corr_size
      resol (for first_pass)
    
    Output:
      vectorfield = the vector matrix for the roi...

    Based on a Matlab function by S. Berujon
    """
       
    szfc = SearchObj()
    # This represent: half * (size-1)
    szfc.st_size = 20
    szfc.sa_size_v = 80
    szfc.sa_size_h = 80
    
    flag_ok = True
    
    # Kept for historical reasons, now only affects non-parallel version
    # To bypass memory issue and distribute over the cores (might not be 
    # necessary)
    sub_div = 12
    
    # Normalize spot images - TEST
    img1 = (img1 - np.ndarray.mean(img1))/np.ndarray.std(img1)
    img2 = (img2 - np.ndarray.mean(img2))/np.ndarray.std(img2)
    
    # Binning
    if (under_sample > 1):
        img1 = img1[::under_sample, ::under_sample]
        img2 = img2[::under_sample, ::under_sample]
    
    # Apply roi to images
    spot1 = roi_select(img1, roi1)
    spot2 = roi_select(img2, roi2)
    
    # Normalize spot images
    spot1_n = (spot1 - np.ndarray.mean(spot1))/np.ndarray.std(spot1)
    spot2_n = (spot2 - np.ndarray.mean(spot2))/np.ndarray.std(spot2)
    
    [m1, n1] = spot1.shape
    [m2, n2] = img2.shape
    
    corr = DEFAULT
    
    # No translation, for differential mode with identical roi
    if (mode is 1):
        #resol = 35
        szfc.sa_size_v = 35
        szfc.sa_size_h = 35
        szfc.sa_offset_v = 0
        szfc.sa_offset_h = 0
    # Small translation, for computing detector distortion
    elif (mode is 2):
        #resol = 35
        szfc.sa_size_v = 65
        szfc.sa_size_h = 65
        filter_Z = 5
    # No assumptions about rigid translation
    else:
        #resol = 10
        filter_Z = 5
    
    if (mode is 2 or mode is 3):
        # Filter to remove shot noise
        spot1_n = avg_filter(spot1_n, filter_Z)
        spot2_n = avg_filter(spot2_n, filter_Z)
        
        spot1_n = spot1_n.astype('float32')
        spot2_n = spot2_n.astype('float32')
        
        # Small subset for computing rigid translation
        spot1_n = spot1_n[int(math.floor(m1/2))-25 : -int(math.floor(m1/2))+26, 
                        int(math.floor(n1/2))-25 : -int(math.floor(n1/2))+26]

        corr, min_val, max_val, min_loc, max_loc = nx.norm_xcorr(spot1_n, 
                                                                 spot2_n)

        disp = nx.nxcorr_disp(corr)

        # x is horizontal (columns), y is vertical (rows)
        if(isinstance(roi1, np.ndarray) and isinstance(roi2, np.ndarray)): 
            szfc.sa_offset_v = round(disp[0]) + round(((roi2[1] - roi2[0]) 
                               - (roi1[1] - roi1[0]))/2.0)
            szfc.sa_offset_h = round(disp[1]) + round(((roi2[3] - roi2[2]) 
                               - (roi1[3] - roi1[2]))/2.0)
        else:
            szfc.sa_offset_v = round(disp[0])
            szfc.sa_offset_h = round(disp[1])

        logging.info('Rigid translation of the beam: ' + str(szfc.sa_offset_v)
                    + ' Vertical pixels and ' + str(szfc.sa_offset_h) 
                    + ' Horizontal pixels')
    
    # FIRST PASS to get an approximation of the displacement
    xi = np.arange(0, n1, resol)
    yi = np.arange(0, m1, resol)
    
    # Querry points for "sub-pixel" interpolation 
    x = np.arange(0, n1, 1)
    y = np.arange(0, m1, 1)
    
    # x and y are switched and messed up somewhere
    # data is in format y, x. check how field1 and field2 are organized.
    
    n_pts_x = len(xi)
    n_pts_y = len(yi)
    
    nPts = n_pts_x * n_pts_y
    
    choice = 0
    
    # SOMETHING WRONG WITH FIRST PASS IF ROIs ARE DIFFERENT
    # PSEUDO rigid translation
    
    if (choice is 1):
        # Parallel version       
        num_cores = multiprocessing.cpu_count()
        
        if (num_cores < 1):
            num_cores = 1
            
        results = Parallel(n_jobs=num_cores)(delayed(first_pass)(
                  (math.floor(k/n_pts_x) * resol), ((k % n_pts_x) * resol), 
                  img1, img2, roi1, roi2, szfc) for k in range(nPts))

        field1 = [item[0] for item in results] 
        field2 = [item[1] for item in results] 
        max_el = [item[2] for item in results] 
    else:    
        # Non-parallel version is actually faster than parallel one 
        # probably because of large data copying overhead - maybe pass 
        # only the part of the images that are needed.
        field1 = np.zeros(nPts)
        field2 = np.zeros(nPts)
        max_el = np.zeros(nPts)
        
        for k in range(nPts):
            # Consider row-major order for numpy
            
            # Select row 
            row = int(math.floor(k/n_pts_x) * resol)
            # Select column
            col = (k % n_pts_x) * resol
            
            (field1[k] ,field2[k] ,max_el[k]) = first_pass(row, col, 
                                                           img1, img2, 
                                                           roi1, roi2, szfc)

    avg_corr_val = np.mean(max_el)
    logging.info('Average value for correlation peak = ' + str(avg_corr_val))

    field1 = np.reshape(field1, (n_pts_x, n_pts_y))
    field2 = np.reshape(field2, (n_pts_x, n_pts_y))
    
    # Correction of the computed value considering the limitation of the 
    # gradient?? - ASK SEB
    field1 = my_erosion(field1, abs(5.0 * np.mean(np.diff(field1,1,0))))
    field2 = my_erosion(field2, abs(5.0 * np.mean(np.diff(field2,1,1))))

    # Approximate by two linear planes
    
    if (mode is 1):
        field1 = field1 * 0.0 + np.median(np.mean(field1,0))
        field2 = field2 * 0.0 + np.median(np.mean(field2,0))
    elif (mode is 2):
        field1 = np.median(field1) * np.ones(field1.shape)
        field2 = np.median(field2) * np.ones(field2.shape)
    else:
        # Use gradient_error = plane_fit
        [field1, slope1_h, slope1_v] = plane_fit(field1, np.ones(field1.shape)) + np.mean(field1)
        [field2, slope2_h, slope2_v] = plane_fit(field2, np.ones(field2.shape)) + np.mean(field2)
    
    # Interpolation -- USING interp2d DIRECTLY
    # possible error: singular matrix error if field1 and 2 are 3x3 or 
    # less
    
    #~ fi = scipy.interpolate.interp1d(xi, field1, kind = 'cubic', axis = 0)
    #~ f = extrap1d(fi, axis = 0)
    #~ zz = f(x)
    #~ fi = scipy.interpolate.interp1d(yi, zz, kind = 'cubic', axis = 1)
    #~ f = extrap1d(fi, axis = 1)
    #~ vec_field1 = f(y)
    #~ vec_field1 = np.around(vec_field1.astype(np.double), 0)
    
    f = scipy.interpolate.interp2d(xi, yi, field1, kind = 'linear')
    vec_field1 = f(x, y)
    
    #~ fi = scipy.interpolate.interp1d(xi, field2, kind = 'cubic', axis = 0)
    #~ f = extrap1d(fi, axis = 0)
    #~ zz = f(x)
    #~ fi = scipy.interpolate.interp1d(yi, zz, kind = 'cubic', axis = 1)
    #~ f = extrap1d(fi, axis = 1)
    #~ vec_field2 = f(y)
    #~ vec_field2 = np.around(vec_field2.astype(np.double), 0)
    
    f = scipy.interpolate.interp2d(xi, yi, field2, kind = 'linear')
    vec_field2 = f(x, y)
    
    logging.info('First Pass finished.')

    # SECOND PASS, more accurate
    xi = np.arange(0, n1, grid_resol)
    yi = np.arange(0, m1, grid_resol)
    
    (x, y) = np.meshgrid(xi,yi)
    
    if(isinstance(roi1, np.ndarray)):
        x_bs = (roi1[2] - 1) + x
        y_bs = (roi1[0] - 1) + y
    else:
        x_bs = x
        y_bs = y
        
    if(isinstance(roi2, np.ndarray)):     
        x_in = (roi2[2] - 1) + x + vec_field2[0 : m1 : grid_resol, 
                                              0 : n1 : grid_resol]
        y_in = (roi2[0] - 1) + y + vec_field1[0 : m1 : grid_resol, 
                                              0 : n1 : grid_resol]
    else:
        x_in = x + vec_field2[0 : m1 : grid_resol, 
                              0 : n1 : grid_resol]
        y_in = y + vec_field1[0 : m1 : grid_resol, 
                              0 : n1 : grid_resol]
    
    # Make sure not outside the boundaries of target image
    x_in[x_in < 0] = 0
    x_in[x_in >= n2] = n2
    y_in[y_in < 0] = 0
    y_in[y_in >= m2] = m2
    
    x_bs = np.reshape(x_bs,(-1,1))
    y_bs = np.reshape(y_bs,(-1,1))
    
    x_in = np.reshape(x_in,(-1,1))
    y_in = np.reshape(y_in,(-1,1))
    
    input_points = np.zeros((len(x_in), 2))
    
    if (len(x_in) > 2000):
        
        choice = 1
        
        if (choice is 1):
            # Parallel version - about 5x faster
            num_cores = multiprocessing.cpu_count()
        
            if (num_cores < 1):
                num_cores = 1
            
            # Prepare number of zones corresponding to number of cores 
            # available
            zone_bounds = np.around(np.linspace(0, len(x_in), num_cores + 1))
            zone_bounds = zone_bounds.astype(int)
            
            out_pts = Parallel(n_jobs=num_cores)(delayed(cp_corr)(
                      corr_size, half_width_fact, np.column_stack((
                      x_in[range(zone_bounds[k], zone_bounds[k + 1], 1)], 
                      y_in[range(zone_bounds[k], zone_bounds[k + 1], 1)])), 
                      np.column_stack((
                      x_bs[range(zone_bounds[k], zone_bounds[k + 1], 1)], 
                      y_bs[range(zone_bounds[k], zone_bounds[k + 1], 1)])), 
                      img2, img1) for k in range(num_cores))
            
            # Update input_points using new results
            for k in range(num_cores):
                zone = range(zone_bounds[k], zone_bounds[k + 1], 1)
                input_points [zone, :] = out_pts[k]
            
        else:
            # Non-parallel version - the sub_div division is here for 
            # "historical reasons"
            zone_bounds = np.around(np.linspace(0, len(x_in), sub_div + 1))
            zone_bounds = zone_bounds.astype(int)
            out_pts = []        
            
            for k in range(sub_div):
                zone = range(zone_bounds[k], zone_bounds[k + 1], 1)
                out_pts.append(cp_corr(corr_size, half_width_fact, 
                                    np.column_stack((x_in[zone], y_in[zone])), 
                                    np.column_stack((x_bs[zone], y_bs[zone])), 
                                    img2, img1))
            
            for k in range(sub_div):
                zone = range(zone_bounds[k], zone_bounds[k + 1], 1)
                input_points [zone, :] = out_pts[k]
            
    else:
        input_points = cp_corr(corr_size, half_width_fact, 
                             np.column_stack((x_in, y_in)), 
                             np.column_stack((x_bs, y_bs)), 
                             img2, img1)
        
    # Second pass, to see if it is correctable
    # REPLACE transpose with getH ??? CHECK!!!
    err_pts = np.where(np.equal(input_points[:, 0], 
                      np.reshape(np.transpose(x_in), -1)) 
                      * np.equal(input_points[:, 1], 
                      np.reshape(np.transpose(y_in), -1)))

    logging.info('For corr_size = ' + str(corr_size) + ': Number of points to correct ' 
                + str(err_pts[0].shape[0]) + ', ' 
                + str(100.0*err_pts[0].shape[0]/input_points.shape[0]) 
                + '%')
    
    # compute percentage of error points
    err_perc = 100.0 * err_pts[0].shape[0]/input_points.shape[0]
    
    corr_size_adj = corr_size
    
    cont = 0
    
    if (err_perc < 50 or cont is 1):
        for corr_size_for in [corr_size_adj + 1, corr_size_adj + 3, corr_size_adj + 6]:
            input_points[err_pts, :] = cp_corr(corr_size_for, half_width_fact, 
                                            np.column_stack((x_in[err_pts], 
                                            y_in[err_pts])), 
                                            np.column_stack((x_bs[err_pts], 
                                            y_bs[err_pts])), 
                                            img2, img1)
            err_pts = np.where(np.equal(input_points[:, 0], 
                              np.reshape(np.transpose(x_in), -1)) 
                              * np.equal(input_points[:, 1], 
                              np.reshape(np.transpose(y_in), -1)))
            err_perc = 100.0 * err_pts[0].shape[0]/input_points.shape[0]
            logging.info('For corr_size = ' + str(corr_size_for) 
                        + ': Number of points to correct ' 
                        + str(err_pts[0].shape[0]) + ', ' + str(err_perc) 
                        + '%')
        
        if (err_perc < 10 or cont is 1):
            for corr_size_for in [corr_size_adj + 10, corr_size_adj + 15]:
                input_points[err_pts, :] = cp_corr(corr_size_for, half_width_fact, 
                                         np.column_stack((x_in[err_pts], 
                                         y_in[err_pts])), 
                                         np.column_stack((x_bs[err_pts], 
                                         y_bs[err_pts])), 
                                         img2, img1)
                err_pts = np.where(np.equal(input_points[:, 0], 
                                  np.reshape(np.transpose(x_in), -1)) 
                                  * np.equal(input_points[:, 1], 
                                  np.reshape(np.transpose(y_in), -1)))
                err_perc = 100.0 * err_pts[0].shape[0] / input_points.shape[0]
                logging.info('For corr_size = ' + str(corr_size_for) 
                            + ': Number of points to correct ' 
                            + str(err_pts[0].shape[0]) + ', ' + str(err_perc) 
                            + '%')
        else:
            flag_ok = False
            
            warn = ('Terminating execution for this pair. More than 10%' 
                    ' points to correct after adjusting corr_size four times'
                    ' in second pass. Consider increasing the input value for'
                    ' corr_size and try again.')
                    
            test_warn(flag_ok, 'cross_spot', warn)
            
            vec_field = 0
            err_pts = 0
            return [vec_field, err_pts, flag_ok]
        
    else:
        flag_ok = False
        
        warn = ('Terminating execution for this pair. More than 50% ' 
                'points to correct after second pass. Consider increasing the '
                'input value for corr_size and try again.')
        
        test_warn(flag_ok, 'cross_spot', warn)
        
        vec_field = 0
        err_pts = 0
        return [vec_field, err_pts, flag_ok]
        
    # Back to rigid translation
    
    vec_field = np.zeros((2, m1, n1))

    vec_field[1, 0 : m1 : grid_resol, 0 : n1 : grid_resol] = np.reshape(
                                                             input_points[:, 0] 
                                                             - x_bs[:,0], 
                                                             x.shape)
    vec_field[0, 0 : m1 : grid_resol, 0 : n1 : grid_resol] = np.reshape(
                                                             input_points[:, 1] 
                                                             - y_bs[:,0], 
                                                             x.shape)
            
    # Mark erroneous area for interpolation
    
    err_pts = np.where(np.equal(input_points[:, 0], 
                      np.reshape(np.transpose(x_in), -1)) 
                      * np.equal(input_points[:, 1], 
                      np.reshape(np.transpose(y_in), -1)))

    x = np.arange(0, n1, grid_resol)
    y = np.arange(0, m1, grid_resol)
    xi = range(n1)
    yi = range(m1)
    
    f = scipy.interpolate.interp2d(x, y, 
                                   vec_field[0, 0 : m1 : grid_resol, 
                                   0 : n1 : grid_resol], 
                                   kind = 'linear')
    vec_field[0, :, :] = f(xi, yi)

    f = scipy.interpolate.interp2d(x, y, 
                                   vec_field[1, 0 : m1 : grid_resol, 
                                   0 : n1 : grid_resol], 
                                   kind = 'linear')
    vec_field[1, :, :] = f(xi, yi)
        
    return [vec_field, err_pts, flag_ok]

def frankot_chellappa(dzdx, dzdy, reflec_pad=True):
    """Python version of FRANKOTCHELLAPPA Matlab code:
    http://www.peterkovesi.com/matlabfns/Shapelet/frankot_chellappa.m

    Copyright notice for original Matlab code:
    -----------------------------------------------------------------------
    FRANKOTCHELLAPPA  - Generates integrable surface from gradients

    An implementation of Frankot and Chellappa'a algorithm for 
    constructing an integrable surface from gradient information.

    Usage:      z = frankot_chellappa(dzdx,dzdy)

    Arguments:  dzdx,  - 2D matrices specifying a grid of gradients of z
                dzdy     with respect to x and y.

    Returns:    z      - Inferred surface heights.

    Reference:

    Robert T. Frankot and Rama Chellappa
    A Method for Enforcing Integrability in Shape from Shading
    IEEE PAMI Vol 10, No 4 July 1988. pp 439-451

    Note this code just implements the surface integration component of 
    the paper (Equation 21 in the paper).  It does not implement their 
    shape from shading algorithm.

    Copyright (c) 2004 Peter Kovesi
    School of Computer Science & Software Engineering
    The University of Western Australia
    http://www.csse.uwa.edu.au/

    Permission is hereby granted, free of charge, to any person 
    obtaining a copy of this software and associated documentation files 
    (the "Software"), to deal in the Software without restriction, 
    subject to the following conditions:

    The above copyright notice and this permission notice shall be 
    included in all copies or substantial portions of the Software.

    The Software is provided "as is", without warranty of any kind.

    October 2004

    Python version written by Ruxandra Cojocaru, July 2017

    Modified frequency domain to correspond to g2s grid, added padding 
    and added factors to equation and reconstruction
    """
        
    test_err(bool(dzdx.shape == dzdy.shape), 'frankot_chellappa', 
            'size of gradient matrices must match')
    
    if reflec_pad:
        dzdx, dzdy = _reflec_pad_grad_fields(dzdx, dzdy)

    (rows, cols) = dzdx.shape
    
    # The following sets up matrices specifying frequencies in the x and 
    # y directions corresponding to the Fourier transforms of the 
    # gradient data.  They range from -0.5 cycles/pixel to 
    # + 0.5 cycles/pixel. The fiddly bits in the line below give the 
    # appropriate result depending on whether there are an even or odd 
    # number of rows and columns
    
    (wx, wy) = np.meshgrid(np.pi * (np.arange(1, cols + 1) 
                           - (np.fix(cols / 2.0) + 1)) 
                           / (cols - np.mod(cols, 2)), 
                           np.pi * (np.arange(1, rows + 1) 
                           - (np.fix(rows / 2.0) + 1)) 
                           / (rows - np.mod(rows, 2)))
        
    # Quadrant shift to put zero frequency at the appropriate edge
    wx = np.fft.ifftshift(wx)
    wy = np.fft.ifftshift(wy)

    # Fourier transforms of gradients
    Fdzdx = np.fft.fft2(dzdx)
    Fdzdy = np.fft.fft2(dzdy)

    # Integrate in the frequency domain by phase shifting by pi/2 and
    # weighting the Fourier coefficients by their frequencies in x and y 
    # and then dividing by the squared frequency.  eps is added to the
    # denominator to avoid division by 0.
    
    # Equation 21 from the Frankot & Chellappa paper
    # ADDED A * (-1)
    Z = (-1 * (-1j * wx * Fdzdx - 1j * wy * Fdzdy) 
         / (wx**2 + wy**2 + np.spacing(1)))
    
    # Reconstruction
    rec = np.real(np.fft.ifft2(Z))
    
    # Source: 
    # http://www.cs.cmu.edu/~ILIM/projects/IM/aagrawal/software.html
    rec = rec/2.0
    
    # Source: 
    # https://github.com/wavepy/wavepy/blob/master/wavepy/surface_from_grad.py
    if reflec_pad:
        return _one_forth_of_array(rec)
    else:
        return rec
    
    return rec
    

def _reflec_pad_grad_fields(del_func_x, del_func_y):
    """Source:
    https://github.com/wavepy/wavepy/blob/master/wavepy/surface_from_grad.py

    Copyright (c) 2015, UChicago Argonne, LLC. All rights reserved.         
                                                                            
    Copyright 2015. UChicago Argonne, LLC. This software was produced       
    under U.S. Government contract DE-AC02-06CH11357 for Argonne 
    National Laboratory (ANL), which is operated by UChicago Argonne, 
    LLC for the U.S. Department of Energy. The U.S. Government has 
    rights to use, reproduce, and distribute this software.  NEITHER THE 
    GOVERNMENT NOR UChicago Argonne, LLC MAKES ANY WARRANTY, EXPRESS OR 
    IMPLIED, OR ASSUMES ANY LIABILITY FOR THE USE OF THIS SOFTWARE.  If 
    software is modified to produce derivative works, such modified 
    software should be clearly marked, so as not to confuse it with the 
    version available from ANL.                                                               
                                                                            
    Additionally, redistribution and use in source and binary forms, 
    with or without modification, are permitted provided that the 
    following conditions are met:                                                     
                                                                            
        * Redistributions of source code must retain the above copyright    
          notice, this list of conditions and the following disclaimer.     
                                                                            
        * Redistributions in binary form must reproduce the above 
          copyright notice, this list of conditions and the following   
          disclaimer in the documentation and/or other materials        
          provided with the distribution.                                                     
                                                                            
        * Neither the name of UChicago Argonne, LLC, Argonne National       
          Laboratory, ANL, the U.S. Government, nor the names of its        
          contributors may be used to endorse or promote products   
          derived from this software without specific prior written 
          permission.     
                                                                            
    THIS SOFTWARE IS PROVIDED BY UChicago Argonne, LLC AND CONTRIBUTORS     
    "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT       
    LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS       
    FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL UChicago     
    Argonne, LLC OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,        
    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,    
    BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;        
    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER        
    CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT      
    LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN       
    ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE         
    POSSIBILITY OF SUCH DAMAGE.                                             

    This fucntion pads the gradient field in order to obtain a 
    2-dimensional reflected function. The idea is that, by having an 
    reflected function, we avoid discontinuity at the edges.
    This was inspired by the code of the function DfGBox, available in 
    theMATLAB File Exchange website:
    https://www.mathworks.com/matlabcentral/fileexchange/45269-dfgbox
    """

    del_func_x_c1 = np.concatenate((del_func_x,
                                    del_func_x[::-1, :]), axis=0)

    del_func_x_c2 = np.concatenate((-del_func_x[:, ::-1],
                                    -del_func_x[::-1, ::-1]), axis=0)

    del_func_x = np.concatenate((del_func_x_c1, del_func_x_c2), axis=1)

    del_func_y_c1 = np.concatenate((del_func_y,
                                    -del_func_y[::-1, :]), axis=0)

    del_func_y_c2 = np.concatenate((del_func_y[:, ::-1],
                                    -del_func_y[::-1, ::-1]), axis=0)

    del_func_y = np.concatenate((del_func_y_c1, del_func_y_c2), axis=1)

    return del_func_x, del_func_y


def _one_forth_of_array(array):
    """
    Undo for the function
    :py:func:`wavepy:surface_from_grad:_reflec_pad_grad_fields`
    """

    array, _ = np.array_split(array, 2, axis=0)
    
    return np.array_split(array, 2, axis=1)[0]


def integ_disto_maps(sdHX, sdVX, integ_meth=1):
    """Function to integrate a pair of gradient maps to obtain the 
    detector distortion map (can be used to obtain either the horizontal 
    or the vertical map)

    You need the gradients sdHX and sdVX, where X can be either V or H

    Based on a Matlab function by S. Berujon
    """
    
    # Integrate
    
    if (integ_meth is 2):
        disto = frankot_chellappa(sdHX, sdVX)
    else:
        (ny, nx) = sdHX.shape
        x = np.linspace(0, nx-1, num = nx)
        y = np.linspace(0, ny-1, num = ny)
        disto = g2s.g2s(x, y, sdHX, sdVX)
        
    # Remove mean
    disto = disto - np.mean(disto)

    return disto

   
def undistort(img, roi, file_horz_disto_map, file_vert_disto_map):
    """Function to undistort image using horizontal and vertical 
    distortion maps
    
    Based on a Matlab function by S. Berujon
    """

    horz_disto_map = load_data(file_horz_disto_map)
    vert_disto_map = load_data(file_vert_disto_map)
    
    if(isinstance(roi, np.ndarray)):
        img1 = np.copy(img[roi[0] : roi[1] + 1, roi[2] : roi[3] + 1])
        xx = np.arange(roi[2], roi[3] + 1, 1)
        yy = np.arange(roi[0], roi[1] + 1, 1)
    else:
        img1 = np.copy(img)
        xx = np.arange(0, img1.shape[1], 1)
        yy = np.arange(0, img1.shape[0], 1)
    
    n = len(xx)
    m = len(yy)
    
    # recently inverted this line
    [Y, X] = np.meshgrid(yy, xx, indexing='ij')

    U1 = horz_disto_map + X
    V1 = vert_disto_map + Y
    
    # recently inverted this line
    f = scipy.interpolate.RegularGridInterpolator((yy, xx), img1, 
                                                  bounds_error = False, 
                                                  method = 'nearest', 
                                                  fill_value = np.mean(img1))
    
    # recently inverted this line
    pts = (np.array([np.ndarray.flatten(V1), np.ndarray.flatten(U1)])
           .transpose())
    img2 = f(pts)
    
    # recently inverted this line
    #~ img2 = np.reshape(img2, (n,m))    
    img2 = np.reshape(img2, (m, n))

    return img2
   

def fill_mask(img0, threshold, ymin=0, ymax=None, xmin=0, xmax=None, 
             method='avg'):
    """Subroutine that fills a region defined by a mask given by a 
    threshold with either the mean of the whole image or the inward 
    integral (similar to MATLAB's fillregion function) 
    """
    
    if (ymax is None):
        ymax = img0.shape[0]
    if (xmax is None):
        xmax = img0.shape[1]

    # A boolean array of (width, height) which False where there are 
    # missing values and True where there are valid (non-missing) values
    mask = (img0[:, :] >= 0)
    mask[ymin:ymax, xmin:xmax] = ~(img0[ymin:ymax, xmin:xmax] > threshold)
    
    if (method == 'avg'):
        img = np.copy(img0)
        img[~mask] = np.mean(img0)
    elif (method == 'integ'):
        # Array of (number of points, 2) containing the x,y coordinates 
        # of the valid values only
        xx, yy = np.meshgrid(np.arange(img.shape[1]), np.arange(img.shape[0]))
        xym = np.vstack( (np.ravel(xx[mask]), np.ravel(yy[mask])) ).T
        # The valid values in the first, second, third color channel,  
        # as 1D arrays (in the same order as their coordinates in xym)
        data = np.ravel(img[:,:][mask])
        # Three separate interpolators for the separate color channels
        interp0 = scipy.interpolate.NearestNDInterpolator(xym, data)
        # Interpolate the whole image, one color channel at a time    
        img = interp0(np.ravel(xx), np.ravel(yy)).reshape(xx.shape)
    else:
        test_err(False, 'fill_mask', 'unknown method; accepted values "avg" '
                 + 'or "integ"')

    return img


def mask_builder(image, mask_half_size):
    """Subroutine to build a circular mask for a lens
    Diameter in pixels of the desired mask = 2 * mask_half_size + 1
    """
    
    err_msg = ('mask_half_size has to be a positive number strictly smaller '
               'than half the shortest dimension of image, ' 
               + str(min(image.shape) / 2))

    test_err(mask_half_size is not None , 'mask_builder', err_msg)

    test_err(mask_half_size > 0 and mask_half_size < min(image.shape) / 2 , 
             'mask_builder', err_msg)
    
    mask_diam = 2 * mask_half_size + 1
    
    [n, m] = image.shape
    
    # BUILD A MASK
    [X1, Y1] = np.meshgrid(np.arange(m), np.arange(n))
    X = X1 - np.round(m/2)
    Y = Y1 - np.round(n/2)
    
    mask0 = np.power(np.square(X) + np.square(Y), 0.5)
    
    exponent = np.exp(-(X**2 + Y**2) / (2 * np.square(mask_diam/2)))

    exponent[mask0 < (mask_diam/2)] = 1
    
    mask1 = (mask0 < (mask_diam/2.*1.3)) * exponent
    
    # sort descending order
    a = -np.sort(-image.reshape(-1))
    
    maskThreshold = sum(mask1[mask1 == 1])/np.size(mask1)

    b = a[int(np.round(len(a) * maskThreshold))]
    
    d1 = (image > b)

    [x_offset,y_offset] = f_shift(d1, mask1)
    
    mask_center = np.array([0, 0])
    mask_center[1] = x_offset + m//2
    mask_center[0] = y_offset + n//2

    # Mask
    big_mask = build_mask(mask_center, mask_half_size, (n,m))
    
    return [big_mask, mask_center]


def build_mask(mask_center, mask_size, grad_shape):
    """Generic function to build a mask around a center using a half_size
    """

    (n, m) = grad_shape

    # Check input center of mask

    try:
        if(mask_center.shape[0] == 2):
            center_y = int(mask_center[0])
            center_x = int(mask_center[1])
            err_msg = ('Center coordinates must be positive and smaller '
                        'than the overall size of the gradient: '
                       + str((n,m)))  
            test_err(center_y > 0 and center_y < n and center_x > 0 
                     and center_x < m, 'build_mask', err_msg)
    except:
        err_msg = ('Center coordinates must be given as a two element array' 
                   ' [center_y, center_x] containing positive numbers;'
                   ' unit: pixels')
        test_err(False, 'build_mask', err_msg)
    
    # Check and interpret input size of mask
    try:
        if (not (type(mask_size) is np.ndarray)):
            mask_size = np.array([mask_size])
        
        if (mask_size.shape[0] == 1 and mask_size is not None):
            mask_type = 'circ'
            mask_diam = int(2 * mask_size[0] + 1)
            err_msg = ('Radius of the mask must be positive and not greater '
                       'than the half the minimum dimension of the ROI = ' 
                       + str(min(n//2, m//2))) 
            test_err(mask_diam > 0 and mask_diam < min(n, m), 
                     'build_mask', err_msg)
        elif (mask_size.shape[0] == 2):
            lens_dim = 1
            mask_type = 'rect'
            rect_y = int(mask_size[0])
            rect_x = int(mask_size[1])
            err_msg = ('Half-size of rectangular mask must be positive and not'
                       ' greater than the half-size of the ROI = '
                       + str([n//2, m//2]))  
            test_err(rect_y > 0 and rect_y < n//2 and rect_x > 0 
                     and rect_x < m//2, 'build_mask', err_msg)
        else:
            err_msg = ('Mask_size must be an array, with either one float '
                       'element -- the radius of the mask, or with two '
                       'elements -- the y and x dimensions of the rectangular '
                       'mask; unit: pixels')
            test_err(False, 'build_mask', err_msg) 
    except:
        err_msg = ('mask_size a number or an array, with either one float '
                   'element -- the radius of the mask, or with two '
                   'elements -- the y and x dimensions of the rectangular '
                   'mask; unit: pixels')
        test_err(False, 'build_mask', err_msg)

    if(mask_type == 'circ'):
        [X1, Y1] = np.meshgrid(np.arange(m), np.arange(n))
        X = X1 - center_x
        Y = Y1 - center_y
        mask0 = np.power(np.square(X) + np.square(Y), 0.5)
        mask2 = (mask0 <= (mask_diam/2))
    else:
        mask2 = np.zeros((n, m))
        mask2[(center_y - mask_size[0]) : (center_y + mask_size[0] + 1),
              (center_x - mask_size[1]) : (center_x + mask_size[1] + 1)] = 1

    mask = (mask2 == True)

    return mask


def f_shift(a, t):
    """Subroutine that determins relative offset between an image and a 
    mask 
    """
    
    # Determine padding size in x and y dimension
    size_t = t.shape
    size_a = a.shape
    out_size = np.array(size_t) + np.array(size_a) - 1

    # Determine 2D cross correlation in Fourier domain
    
    Ft = np.fft.fft2(t, s = (out_size[0], out_size[1]))
    Fa = np.fft.fft2(a, s = (out_size[0], out_size[1]))
    
    c = abs(np.fft.fftshift(np.fft.ifft2(Fa * np.conj(Ft))))
    
    # Find peak, np.argmax return the linear index of 1st occurence of 
    # max.
    [max_c, imax] = [np.max(c), np.argmax(c)]
    
    [y_peak, x_peak]  = np.unravel_index(imax, c.shape)
    
    # Correct found peak location for image size
    #~ corr_offset = np.array((y_peak + 1 - c.shape[0] // 2, x_peak + 1 - c.shape[1] // 2))
    corr_offset = np.array((y_peak - c.shape[0] // 2, x_peak - c.shape[1] // 2))
    
    # Write out offsets
    y_offset = corr_offset[0]
    x_offset = corr_offset[1]
    
    return [x_offset,y_offset]
    
    
def save_plot(data, file_name, dir_out, sub_dir='', xlabel=None,  
              ylabel=None, zlabel = None, clabel = None, 
              plot_type='2D', pixel_size = None):
    """Subroutine that plots a data array and saves it to a file
    """
    
    # test plot_type variable, can only be 2D or 3D
    
    fig = plt.figure()
    
    if (plot_type == '3D'):
        ax = fig.gca(projection='3d')

        (ny, nx) = data.shape
        
        data_max = max(data.flatten())
        data_min = min(data.flatten())

        x = (np.arange(np.ceil(-nx/2.0), np.ceil(nx/2.0), 1) 
             * pixel_size * 10**(-3))
        y = (np.arange(np.ceil(-ny/2.0), np.ceil(ny/2.0), 1) 
             * pixel_size * 10**(-3))

        X, Y = np.meshgrid(x, y)

        surf = ax.plot_surface(X, Y, data, cmap=cm.coolwarm,  
                               linewidth=0, antialiased = False)
            
        # Customize the x,y,z axis.
        if (xlabel):
            ax.set_xlabel('\n ' + xlabel, linespacing=3.2)
        if (ylabel):
            ax.set_ylabel('\n ' + ylabel, linespacing=3.1)
        
        ax.set_zlim(0, data_max)
        
        if (zlabel):
            ax.zaxis.set_major_locator(LinearLocator(10))
            ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))
            #~ ax.set_zlabel('\n ' + zlabel, linespacing=3.4)
            ax.set_zlabel('\n ' + zlabel, linespacing=1)

        # Add a color bar which maps values to colors.
        cbar = plt.colorbar(surf, shrink=0.5, aspect=5)
        if (clabel):
            cbar.set_label(clabel)
            
    elif (plot_type == '1D'):
        if(type(data) is list or type(data) is tuple and len(data) > 1):
            plt.plot(data[0], data[1])
        else:
            plt.plot(data[0], data[1])

                # Customize the x,y labels.
        if (xlabel):
            plt.xlabel('\n ' + xlabel, linespacing=3.2)

        if (ylabel):
            plt.ylabel('\n ' + ylabel, linespacing=3.1)
        
    else: #2D
        new_min = np.min(data[np.abs(data - np.mean(data)) < 2 * np.std(data)])
        new_max = np.max(data[np.abs(data - np.mean(data)) < 2 * np.std(data)])
        plt.imshow(data, cmap=cm.coolwarm, vmin=new_min, vmax=new_max)
        cbar = plt.colorbar()
        
        # Customize the x,y labels.
        if (xlabel):
            plt.xlabel('\n ' + xlabel, linespacing=3.2)

        if (ylabel):
            plt.ylabel('\n ' + ylabel, linespacing=3.1)

        if (clabel):
            cbar.set_label(clabel)
    
    plt.savefig(os.path.join(dir_out, sub_dir, file_name + '.png'))
    plt.close(fig)
    

def save_data(table, file_name, dir_out, subdir, output_type = 'txt', 
              header = ''):
    '''Save output data in desired format: txt, mat, npy
    Input file_name without extension
    Return True is case of success, throw error in case of failure
    Header works only for txt output type
    '''
    
    dir_out = os.path.abspath(dir_out)
    subdir_path = os.path.join(dir_out, subdir)
    
    # make dir_out/subdir if they do not exist
    if (not os.path.exists(subdir_path)):
        warn = ('Path ' + str(subdir_path) + ' does not exist on disk. '
                + 'Replacing with None.')
        test_warn(False, 'save_data', warn)
        try:
            os.makedirs(subdir_path)
        except:
            write_err = ('Cannot create ' + str(subdir_path) + '. Check '
                          ' write permissions')
            test_err(False, 'save_data', write_err)
    
    file_path = os.path.join(subdir_path, file_name)
    
    success = True
    
    if(output_type == 'txt'):
        file_path = file_path + '.txt'
        np.savetxt(file_path, table, fmt = '%.18e', delimiter = '\t', 
                   newline = '\n', header = header)
        # load with np.loadtxt(file_path)
    elif(output_type == 'mat'):
        file_path = file_path + '.mat'
        # Create a dictionary
        dictio = {}
        dictio[file_name] = table
        sio.savemat(file_path, dictio)
        # load with load(file_path) in Matlab
    elif(output_type == 'npy'):
        file_path = file_path + '.npy'
        np.save(file_path, table)
        # load with np.load(file_path)
    else:
        test_err(False, 'save_data', 'Unknown output type' + str(output_type) 
                 + '. Data cannot be saved')
        success = False
        
    return success


def load_data(file_path):
    '''Load data that was saved in format txt or npy
    Determine data type from file name suffix
    Return loaded data array
    '''
    
    file_path = os.path.abspath(file_path)
    
    if (file_path.endswith('npy')):
        table = np.load(file_path)
    else:
        try:
            table = np.loadtxt(file_path)
        except:
            test_err(False, 'load_data', 'Cannot load. Unknown data type for ' 
                    + 'file:' + str(file_path))
    return table
