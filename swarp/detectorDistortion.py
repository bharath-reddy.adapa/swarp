#!/bin/python

#/*##########################################################################
#
# SWaRP: Speckle Wavefront Reconstruction Package 
#
# Copyright (c) 2016-2018 European Synchrotron Radiation Facility
#
# This file is part of the SWaRP Speckle Wavefront Reconstruction Package
# developed at the ESRF by the staff of BM05 as part of EUCALL WP7:PUUCA.
#
# This project has received funding from the European Union’s Horizon 2020 
# research and innovation programme under grant agreement No 654220.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
#############################################################################*/

import sys
import os
from os import walk
import math
import numpy as np
import scipy.interpolate
# Remove X11 dependency                                                                                                              
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import h5py
import time
import datetime
import logging
from logging.handlers import RotatingFileHandler
from logging import handlers
# Non-standard python libraries
from EdfFile import EdfFile
import func
import norm_xcorr


# Define functions

# Range function variation with end included
range1 = lambda start, end: range(start, end+1)

def main():
    
    # Get time stamp
    ts = time.time()
    dt = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d_%H:%M:%S')
    
    # Initializing logging
    log = logging.getLogger('')
    log.setLevel(logging.INFO)
    format = logging.Formatter('%(levelname)s: %(message)s')
    
    ch = logging.StreamHandler(sys.stdout)
    ch.setFormatter(format)
    log.addHandler(ch)

    # Read images and ini file paths from command line

    error_msg = ('Script call must be: script.py path_to_files file.ini [...]')

    if(not func.test_err(len(sys.argv) >= 2, error_msg)):
        directory = os.path.abspath(sys.argv[1])
        path_ini = os.path.abspath(sys.argv[2])

    error_msg = 'Directory ' + str(directory) + 'does not exist on disk'
    func.test_err(os.path.exists(directory), error_msg)

    error_msg = 'File ' + str(path_ini) + 'does not exist on disk'
    func.test_err(os.path.exists(path_ini), error_msg)

    # Read input parameters from ini file
    section = 'detectorDistortion.config'
    param = func.check_input(section, path_ini, dt)
    
    # Start also saving log information to file
    fh = handlers.RotatingFileHandler(os.path.join(param['dir_out'], 
                                      'detectorDistortion_' + dt + '.log'), 
                                      maxBytes=(1048576*5), backupCount=7)
    fh.setFormatter(format)
    log.addHandler(fh)

    if(param['subdir_data']):
        if(param['prefix_files'] is None):
            prefix = param['prefix_files'] + '_'
        else:      
            prefix = ''
            
        file_h1 = os.path.join(param['dir_out'], param['subdir_data'], 
                               prefix + 'sd_hv.dat')
        file_h2 = os.path.join(param['dir_out'], param['subdir_data'], 
                               prefix + 'sd_hh.dat')
        file_h3 = os.path.join(param['dir_out'], param['subdir_data'], 
                               prefix + 'pix_size_h.dat')
        file_h4 = os.path.join(param['dir_out'], param['subdir_data'], 
                               prefix + 'horz-ss_v.dat')
        file_h5 = os.path.join(param['dir_out'], param['subdir_data'], 
                               prefix + 'horz-ss_h.dat')
        file_h6 = os.path.join(param['dir_out'], param['subdir_data'], 
                               prefix + 'disto_h.dat')
        
        file_v1 = os.path.join(param['dir_out'], param['subdir_data'], 
                               prefix + 'sd_vv.dat')
        file_v2 = os.path.join(param['dir_out'], param['subdir_data'], 
                               prefix + 'sd_vh.dat')
        file_v3 = os.path.join(param['dir_out'], param['subdir_data'], 
                               prefix + 'pix_size_v.dat')
        file_v4 = os.path.join(param['dir_out'], param['subdir_data'], 
                               prefix + 'vert-ss_v.dat')
        file_v5 = os.path.join(param['dir_out'], param['subdir_data'], 
                               prefix + 'vert-ss_h.dat')
        file_v6 = os.path.join(param['dir_out'], param['subdir_data'], 
                               prefix + 'disto_v.dat')

    # Open files for dark and flat field images
    # Use EdfFile library to extract header and image data

    # Obtain image for Dark Field correction
    [dark, have_dark, dark_avg] = func.compute_corr_field(
                                  param['path_dark'], 
                                  param['prefix_dark'], 
                                  param['file_type'], 
                                  'Dark', 'median')

    # Obtain image for Flat Field correction
    [flat, have_flat, flat_avg] = func.compute_corr_field(
                                  param['path_flat'], 
                                  param['prefix_flat'], 
                                  param['file_type'], 
                                  'Flat', 'average')
    
    if (have_flat != 0):
        fig = plt.figure()
        plt.imshow(flat, cmap='gray')
        plt.colorbar()
        plt.savefig(os.path.join(param['dir_out'], 'flat_avg.png'))
        plt.close(fig)

    # Open directory and open all files starting with prefix

    file_list = []

    for (dir_path, dir_names, file_names) in walk(directory):
        file_list.extend(file_names)
        break

    # Use EdfFile library to get header &image data from files in directory

    # Select only file in directory that start with prefix (if given)
    if(param['prefix_files']):
        file_list = [x for x in file_list 
                     if x.startswith(param['prefix_files'])]
        #~ file_list = filter(lambda x: x.startswith(
                           #~ param['prefix_files']), 
                           #~ file_list)

    logging.info(str(len(file_list)) + ' files')

    # Sort list of files by filename
    file_list = sorted(file_list)

    # Check if there are files in folder with given prefix and that the 
    # total number of mesh scan files is N**2

    n_images = len(file_list)
    rtn_images = int(math.sqrt(n_images))

    error_msg = ('No mesh scan files found. Check directory path or '
                 'prefix string in input parameters.')
    func.test_err(n_images > 0, 'detectorDistortion', error_msg)

    error_msg = ('Total number of mesh scan files should be a perfect '
                 'square, NxN.')
    func.test_err(n_images == (rtn_images**2), 'detectorDistortion', error_msg)

    ax = np.array([1, 0])

    if(param['fast_axis'] == 2):
        ax = np.array([0, 1])
    
    # Timing start
    start = time.time()

    # Run twice, once for each orientation (horizontal - 1, vertical - 2)
    for direction in [1, 2]:
        
        # Prepare image selection array according to inter_corr_step and 
        # orientation

        # Compare direction with FAST AXIS and prepare image selection array 
        # according to param['inter_corr_step'] and orientation
        
        [ksel, inter_corr_step0] = func.setup_motor_mesh(direction, 
                                   param['fast_axis'], 
                                   param['inter_corr_step'], 
                                   n_images)
        
        logging.info('Direction: ' + str(direction))
        logging.info('Fast axis: ' + str(param['fast_axis']))
        logging.info('Grid: ' + str(ksel))
        logging.info('inter_corr_step = ' + str(param['inter_corr_step']))
        
        # Loop through all images to pass cross_spot4 algorithm twice 
        # (back and forth)
        speck_dis1 = []
        speck_dis2 = []

        for k in ksel:
            # Read images and do dark and flat field corrections if possible
            
            #TODO adapt for NonEDF files!!
            
            image_file1 = EdfFile(os.path.join(directory, file_list[k]))
            image_file2 = EdfFile(os.path.join(directory, 
                                  file_list[k + inter_corr_step0]))
            
            image1 = image_file1.GetData(0).astype('float32')
            image2 = image_file2.GetData(0).astype('float32')
            
            # Dark and Flat Field correction   
            image1 = func.correction(image1, have_flat, flat, have_dark, dark)
            image2 = func.correction(image2, have_flat, flat, have_dark, dark)
            
            if (param['roi_mode'] == 'full'):
                param['roi_default'] = [1, image1.shape(0), 1, image1.shape(1)]
                
            if (param['roi_mode'] == 'auto'):
                if(have_dark):
                    dark_avg = np.average(dark)
                else:
                    dark_avg=100
                param['roi_default'] = roi_auto(image1, dark_avg)
            
            # Use Python lists as cell arrays
            
            logging.info('File pair: ' + str(file_list[k + inter_corr_step0])  
                        + ' and '+ str(file_list[k]))
                  
            speck_dis2.append(func.cross_spot(image2, image1, 
                                              param['roi_mode'], 
                                              param['roi_default'], 
                                              param['roi_default'], 
                                              param['grid_resol'], 
                                              param['mode'], 
                                              param['corr_size'], 
                                              param['resol_first_pass'], 
                                              param['half_width_fact'], 
                                              param['under_sample'], 
                                              param['dir_out'], 
                                              file_list[k + inter_corr_step0], 
                                              file_list[k])[0])
            
            logging.info('File pair: ' + str(file_list[k]) + ' and ' 
                        + str(file_list[k + inter_corr_step0]))
                  
            speck_dis1.append(func.cross_spot(image1, image2, 
                                              param['roi_mode'], 
                                              param['roi_default'], 
                                              param['roi_default'], 
                                              param['grid_resol'], 
                                              param['mode'], 
                                              param['corr_size'], 
                                              param['resol_first_pass'], 
                                              param['half_width_fact'], 
                                              param['under_sample'], 
                                              param['dir_out'], 
                                              file_list[k], 
                                              file_list[k + inter_corr_step0])[0])

        siz = speck_dis1[0][:][:][:].shape

        if (param['avg'] == 'median'):
            ss_v = np.zeros([len(ksel), siz[1], siz[2]])
            ss_h = np.zeros([len(ksel), siz[1], siz[2]])
            
            for k in range(len(speck_dis1)):
                # Central difference
                ss_v[k, :, :] = (speck_dis1[k][0][:][:] 
                                - speck_dis2[k][0][:][:]) / 2.
                ss_h[k, :, :] = (speck_dis1[k][1][:][:] 
                                - speck_dis2[k][1][:][:]) / 2.
              
            # Sort vector
            ss_v = np.sort(ss_v, axis=0)
            ss_h = np.sort(ss_h, axis=0)
            
            #Remove first and last third
            
            vi = int(ss_v.shape[0]/3)
            hi = int(ss_h.shape[0]/3)
            
            vf = int(2*ss_v.shape[0]/3 + 1)
            hf = int(2*ss_h.shape[0]/3 + 1)    
            
            ss_v = ss_v[vi:vf, :, :]
            ss_h = ss_h[hi:hf, :, :]
            
            ##CHANGE: take average of median values -- HOW??
            ss_v = np.median(ss_v, axis = 0)
            ss_h = np.median(ss_h, axis = 0)
            
        if (param['avg'] == 'average'):
            ss_v = np.zeros([siz[1], siz[2]])
            ss_h = np.zeros([siz[1], siz[2]])
            
            for k in range(len(speck_dis1)):
                # Central difference
                ss_v = ss_v + (speck_dis1[k][0][:][:] 
                       - speck_dis2[k][0][:][:]) / 2.
                ss_h = ss_h + (speck_dis1[k][1][:][:] 
                       - speck_dis2[k][1][:][:]) / 2.
            
            ss_v = ss_v / float(len(speck_dis1))
            ss_h = ss_h / float(len(speck_dis1))
                
        if (direction is 1):
            # For HORIZONTAL orientation
            
            pix_size_h = np.abs(param['motor_step'] * 1000. / np.mean(ss_h))

            sd_hh = (ss_h - np.mean(ss_h)) / np.mean(ss_h)
            # Also remove mean of crossterm from crossterm to compensate for 
            # missalignment between translation motor and camera pixel array
            sd_hv = ss_v/ np.mean(ss_h)
            vert_pix_drift = np.mean(sd_hv)
            sd_hv = sd_hv - vert_pix_drift
            
            # Vertical pixel size corrected for misalignment
            pix_size_h_corr = np.sqrt(pix_size_h**2.0 + vert_pix_drift**2.0)   
            
            if(param['subdir_data']):
                np.savetxt(file_h1, sd_hv, fmt = '%.18e', delimiter = '\t', 
                           newline = '\n')
                np.savetxt(file_h2, sd_hh, fmt = '%.18e', delimiter = '\t', 
                           newline = '\n')
                
                # Uncorrected Pixel Size, Corrected Pixel Size, Misalignment 
                # Angle in Degrees
                np.savetxt(file_h3, np.array([pix_size_h, 
                           pix_size_h_corr, math.degrees(math.acos(
                           pix_size_h/pix_size_h_corr))]))
                np.savetxt(file_h4, ss_v, fmt = '%.18e', delimiter = '\t', 
                           newline = '\n')
                np.savetxt(file_h5, ss_h, fmt = '%.18e', delimiter = '\t', 
                           newline = '\n')
            
        elif (direction == 2):
            # For VERTICAL orientation
                       
            pix_size_v = np.abs(param['motor_step'] * 1000. 
                       / np.mean(ss_v))
                       
            sd_vv = (ss_v - np.mean(ss_v)) / np.mean(ss_v)
            
            # Also remove mean of crossterm from crossterm to compensate for  
            # missalignment between translation motor and camera pixel array
            sd_vh = ss_h / np.mean(ss_v)
            horz_pix_drift = np.mean(sd_vh)
            sd_vh = sd_vh - horz_pix_drift
            
            # Vertical pixel size corrected for misalignment
            pix_size_v_corr = np.sqrt(pix_size_v**2.0 + horz_pix_drift**2.0)
            
            # Save to file
            if(param['subdir_data']):
                np.savetxt(file_v1, sd_vv, fmt = '%.18e', delimiter = '\t', 
                           newline = '\n')
                np.savetxt(file_v2, sd_vh, fmt = '%.18e', delimiter = '\t', 
                           newline = '\n')
                # Uncorrected Pixel Size, Corrected Pixel Size, Misalignment 
                # Angle in Degrees
                np.savetxt(file_v3, np.array([pix_size_v, 
                           pix_size_v_corr, math.degrees(math.acos(
                           pix_size_v/pix_size_v_corr))]))
                np.savetxt(file_v4, ss_v, fmt = '%.18e', delimiter = '\t', 
                           newline = '\n')
                np.savetxt(file_v5, ss_h, fmt = '%.18e', delimiter = '\t', 
                           newline = '\n')
                           
    # Timing end
    end = time.time()
    logging.info(str(end - start) + ' seconds elapsed')
    logging.info(str((end - start)/60.0) + ' minutes elapsed')

    logging.info('Horizontal pixel size: ' + str(pix_size_h_corr) 
                + '\t Misalignment angle in deg: ' 
                + str(math.degrees(math.acos(pix_size_h/pix_size_h_corr))))
    logging.info('Vertical pixel size: ' + str(pix_size_v_corr) 
                + '\t Misalignment angle in deg: ' 
                + str(math.degrees(math.acos(pix_size_v/pix_size_v_corr))))
    logging.info('Final pixel size: ' 
                + str((pix_size_h_corr + pix_size_v_corr) / 2.0))

    file_pix = os.path.join(param['dir_out'], param['subdir_data'], 
                            param['prefix_files'] + '_pix_size.dat')
    np.savetxt(file_pix, np.array([(pix_size_h_corr + pix_size_v_corr) / 2.0]))

    if(param['integ_meth'] is 1):
        logging.info('Integrating horizontal distortion map with FC')
        disto_h = (-1) * func.integ_disto_maps(sd_hh, sd_vh, integ_meth = 2)
        disto_h = disto_h - np.mean(disto_h)
    
        logging.info('Integrating vertical distortion map with FC')
        disto_v = (-1) * func.integ_disto_maps(sd_hv, sd_vv, integ_meth = 2)
        disto_v = disto_v - np.mean(disto_v)

        if (param['subdir_fig']):
            func.save_plot(disto_h, '2D_disto_h_FC', 
                           param['dir_out'], xlabel='pixels',  
                           ylabel='pixels',
                           sub_dir=param['subdir_fig'], clabel='pixels')
        
            func.save_plot(disto_v, '2D_disto_v_FC', 
                           param['dir_out'], xlabel='pixels',  
                           ylabel='pixels',
                           sub_dir=param['subdir_fig'], clabel='pixels')
    
    else:
        logging.info('Integrating horizontal distortion map with grad2surf')
        disto_h = func.integ_disto_maps(sd_hh, sd_vh, integ_meth = 1)
        disto_h = disto_h - np.mean(disto_h)

        logging.info('Integrating vertical distortion map with grad2surf')
        disto_v = func.integ_disto_maps(sd_hv, sd_vv, integ_meth = 1)
        disto_v = disto_v - np.mean(disto_v)

    if(param['subdir_data']):                               
        np.savetxt(file_h6, disto_h, fmt = '%.18e', 
                   delimiter = '\t', newline = '\n')
                               
        np.savetxt(file_v6, disto_v, fmt = '%.18e', 
                   delimiter = '\t', newline = '\n')

    if(param['subdir_fig']):
        func.save_plot(disto_h, '2D_disto_h_g2s', 
                       param['dir_out'], xlabel='pixels',  
                       ylabel='pixels',
                       sub_dir=param['subdir_fig'], clabel='pixels')
                       
        func.save_plot(disto_v, '2D_disto_v_g2s', 
                       param['dir_out'], xlabel='pixels',  
                       ylabel='pixels',
                       sub_dir=param['subdir_fig'], clabel='pixels')

    logging.info('Computation finished')
    
    
if __name__ == '__main__':
    
    main()
            

