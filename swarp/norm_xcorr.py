#/*##########################################################################
#
# SWaRP: Speckle Wavefront Reconstruction Package 
#
# Copyright (c) 2016-2018 European Synchrotron Radiation Facility
#
# This file is part of the SWaRP Speckle Wavefront Reconstruction Package
# developed at the ESRF by the staff of BM05 as part of EUCALL WP7:PUUCA.
#
# This project has received funding from the European Union’s Horizon 2020 
# research and innovation programme under grant agreement No 654220.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
#############################################################################*/

__author__ = 'Ruxandra Cojocaru, Sebastien Berujon'
__contact__ = 'cojocaru@esrf.fr; sebastien.berujon@esrf.fr'
__license__ = 'MIT'
__copyright__ = 'European Synchrotron Radiation Facility, Grenoble, France'
__date__ = '27/08/2018'

import cv2
import numpy as np


def norm_xcorr(template, search_area, method='cv2.TM_CCOEFF_NORMED'):
    """Compute correlation map between template and search area
    Uses matchTemplate from OpenCV: 
    http://docs.opencv.org/3.1.0/d4/dc6/tutorial_py_template_matching.html
    """
    
    meth = eval(method)
    
    corr = cv2.matchTemplate(search_area, template, meth)
    
    min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(corr)
    
    return corr, min_val, max_val, min_loc, max_loc
 

def nxcorr_disp(nxcorr):
    """Find displacement in correlation map

    Written by R. Cojocaru, November 2016; Updated January 2017
    
    Adapted from gaussiansubpixel.cpp by Zachary Taylor:
    https://github.com/OpenPIV/openpiv-c--qt/blob/master/src/gaussiansubpixel.cpp
    and a also a code by S. Berujon

    Also deals with the cases in which the max. value is on the border 
    of the map, by searching for the maximum only within an inner area 
    that excludes the border

    !!The max_value should be updated in the amplitude parameter after 
    running this
    """
    
    length_y = nxcorr.shape[0]
    length_x = nxcorr.shape[1]

    max_value = -1000.0
    
    # Find maximum value in correlation matrix and its position
    # In order to avoid border effects, we search from 1 to length-2 
    # included
    # FOR SEB: THIS IS WHERE I "ignore" the 1 pixel frame when searching 
    # for max
    for i in range(1,length_y-1):
        for j in range(1,length_x-1):
            if (nxcorr[i, j] > max_value):
                max_value = nxcorr[i, j]
                max_i = i
                max_j = j
                
    # Calculate the average to which to compare the signal
    avg = 0.0
    count = 0
    
    for i in range(length_y):
        for j in range(length_x):
            if ((i is not max_i) and (j is not max_j)):
                avg = avg + abs(nxcorr[i,j])
                count = count + 1

    avg = avg / float(count)
    
    # Assign the signal-to-noise (snr) value
    SN_ratio = max_value / avg
    
    # Compute displacement on both axes
    
    dy = (nxcorr[max_i + 1, max_j] - nxcorr[max_i - 1, max_j]) / 2.0
    dyy = (nxcorr[max_i + 1, max_j] + nxcorr[max_i - 1, max_j] 
           - 2.0 * nxcorr[max_i, max_j])

    dx = (nxcorr[max_i, max_j + 1] - nxcorr[max_i, max_j - 1]) / 2.0
    dxx = (nxcorr[max_i, max_j + 1] + nxcorr[max_i, max_j - 1] 
           - 2.0 * nxcorr[max_i, max_j])

    dxy = (nxcorr[max_i + 1, max_j + 1] - nxcorr[max_i + 1, max_j - 1] 
           - nxcorr[max_i - 1, max_j + 1] + nxcorr[max_i - 1, max_j - 1]) / 4.0
    
    if ((dxx * dyy - dxy * dxy) != 0.0):
        det = 1.0 / (dxx * dyy - dxy * dxy)
    else:
        det = 0.0
    
    ix = - (dyy * dx - dxy * dy) * det + max_j - float(length_x//2)    
    iy = - (dxx * dy - dxy * dx) * det + max_i - float(length_y//2)
    
    out = []
    # vertical, then horizontal
    out.append(iy)
    out.append(ix)
    out.append(SN_ratio)
    out.append(max_value)
    
    return out


def nxcorr_disp_old(nxcorr):
    """Find displacement in correlation map

    Written by R. Cojocaru, November 2016
    Adapted from gaussiansubpixel.cpp by Zachary Taylor:
    https://github.com/OpenPIV/openpiv-c--qt/blob/master/src/gaussiansubpixel.cpp
    and a also a code by S. Berujon

    Also deals with the cases in which the max. value is on the border 
    of the map, by padding with 0s around the borders
    """
    
    length_y = nxcorr.shape[0]
    length_x = nxcorr.shape[1]
    
    max_value = -1000.0
    
    # Find maximum value in correlation matrix and its position
    for i in range(length_y):
        for j in range(length_x):
            if (nxcorr[i, j] > max_value):
                max_value = nxcorr[i, j]
                max_i = i
                max_j = j
                
    # Calculate the average to which to compare the signal
    avg = 0.0
    count = 0
    
    for i in range(length_y):
        for j in range(length_x):
            if ((i is not max_i) and (j is not max_j)):
                avg = avg + abs(nxcorr[i,j])
                count = count +1

    avg = avg / float(count)
    
    # Assign the signal-to-noise (snr) value
    SN_ratio = max_value / avg

    # Gaussian fitting to the peak in the correlation map
    f0 = abs(nxcorr[max_i, max_j])
    if (f0 > 0.0):
        f0 = np.log(f0)
    else:
        f0 = 0.0
    
    # Assign the vertical displacement
    if(max_i > 0 and max_i < (length_y - 1)):
        f1 = abs(nxcorr[max_i - 1, max_j])
        if (f1 > 0.0):
            f1 = np.log(f1)
        else:
            f1 = 0.0
            
        f2 = abs(nxcorr[max_i + 1, max_j])
        if (f2 > 0.0):
            f2 = np.log(f2)
        else:
            f2 = 0.0
        
        if(abs(2.0 * f1 - 4.0 * f0 + 2.0 * f2) > 0.0):
            #removed + 1 from max_i
            y_disp = float(length_y//2) - (max_i + (f1 - f2) 
                    / (2.0 * f1 - 4.0 * f0 + 2.0 * f2))
        else:
            y_disp = 0.0
    else:
        if(max_i == 0):
            f1 = 0.0
        else:
            f1 = abs(nxcorr[max_i - 1, max_j])
            if (f1 > 0.0):
                f1 = np.log(f1)
            else:
                f1 = 0.0
        if(max_i == (length_y - 1)):
            f2 = 0.0
        else:    
            f2 = abs(nxcorr[max_i + 1, max_j])
            if (f2 > 0.0):
                f2 = np.log(f2)
            else:
                f2 = 0.0
        
        if(abs(2.0 * f1 - 4.0 * f0 + 2.0 * f2) > 0.0):
            #removed + 1 from max_i  
            y_disp = float(length_y//2) - (max_i + (f1 - f2) 
                    / (2.0 * f1 - 4.0 * f0 + 2.0 * f2))
        else:
            y_disp = 0.0
        
    # Assign the horizontal displacement
    if(max_j > 0 and max_j < (length_x - 1)):
        f1 = abs(nxcorr[max_i, max_j - 1])
        if (f1 > 0.0):
            f1 = np.log(f1)
        else:
            f1 = 0.0
            
        f2 = abs(nxcorr[max_i, max_j + 1])
        if (f2 > 0.0):
            f2 = np.log(f2)
        else:
            f2 = 0.0
        
        if(abs(2.0 * f1 - 4.0 * f0 + 2.0 * f2) > 0.0):   
            #removed + 1 from max_j   
            x_disp = float(length_x//2) - (max_j + (f1 - f2) 
                    / (2.0 * f1 - 4.0 * f0 + 2.0 * f2))
        else:
            x_disp = 0.0
    else:
        if(max_j == 0):
            f1 = 0.0
        else:
            f1 = abs(nxcorr[max_i, max_j - 1])
            if (f1 > 0.0):
                f1 = np.log(f1)
            else:
                f1 = 0.0
        if(max_j == (length_x - 1)):
            f2 = 0.0
        else:
            f2 = abs(nxcorr[max_i, max_j + 1])
            if (f2 > 0.0):
                f2 = np.log(f2)
            else:
                f2 = 0.0
        
        if(abs(2.0 * f1 - 4.0 * f0 + 2.0 * f2) > 0.0):
            #removed + 1 from max_j    
            x_disp = float(length_x//2) - (max_j + (f1 - f2) 
                    / (2.0 * f1 - 4.0 * f0 + 2.0 * f2))
        else:
            x_disp = 0.0
        
    out = [None] * 5
    
    # vertical, then horizontal
    out[3] = -y_disp
    out[4] = -x_disp
    out[2] = SN_ratio
    
    if (max_i > 0 and max_i < (length_y - 1)):
        dy = (nxcorr[max_i + 1, max_j] - nxcorr[max_i - 1, max_j]) / 2.0
        dyy = (nxcorr[max_i + 1, max_j] + nxcorr[max_i - 1, max_j] 
               - 2.0 * nxcorr[max_i, max_j])
    elif (max_i == 0):
        dy = (nxcorr[max_i + 1, max_j] - 0.0) / 2.0
        dyy = nxcorr[max_i + 1, max_j] + 0.0 - 2.0 * nxcorr[max_i, max_j]
    else: 
        dy = (0.0 - nxcorr[max_i - 1, max_j]) / 2.0
        dyy = 0.0 + nxcorr[max_i - 1, max_j] - 2.0 * nxcorr[max_i, max_j]
    # -----------------------------------------  
    if (max_j > 0 and max_j < (length_x - 1)):    
        dx = (nxcorr[max_i, max_j + 1] - nxcorr[max_i, max_j - 1]) / 2.0
        dxx = (nxcorr[max_i, max_j + 1] + nxcorr[max_i, max_j - 1] 
               - 2.0 * nxcorr[max_i, max_j])
    elif (max_j == 0):
        dx = (nxcorr[max_i, max_j + 1] - 0.0) / 2.0
        dxx = nxcorr[max_i, max_j + 1] + 0.0- 2.0 * nxcorr[max_i, max_j]
    else:
        dx = (0.0 - nxcorr[max_i, max_j - 1]) / 2.0
        dxx = 0.0 + nxcorr[max_i, max_j - 1] - 2.0 * nxcorr[max_i, max_j]   
    # -----------------------------------------   
    if (max_i > 0 and max_i < (length_y - 1) and max_j > 0 and 
        max_j < (length_x - 1)):    
        dxy = ((nxcorr[max_i + 1, max_j + 1] - nxcorr[max_i + 1, max_j - 1] 
                - nxcorr[max_i - 1, max_j + 1] + nxcorr[max_i - 1, max_j - 1]) 
                / 4.0)
    else:
        if (max_i < (length_y - 1) and max_j < (length_x - 1)):
            PiPj = nxcorr[max_i + 1, max_j + 1]
        else:
            PiPj = 0.0
        # ------------------------------------      
        if (max_i > 0 and max_j > 0):
            MiMj = nxcorr[max_i - 1, max_j - 1]
        else:
            MiMj = 0.0
        # ------------------------------------  
        if (max_i > 0 and max_j < (length_x - 1)):
            MiPj = nxcorr[max_i - 1, max_j + 1]
        else:
            MiPj = 0.0
        # ------------------------------------
        if (max_i < (length_y - 1) and max_j > 0):
            PiMj = nxcorr[max_i + 1, max_j - 1]
        else:
            PiMj = 0.0
        # ------------------------------------    
        dxy = (PiPj - PiMj - MiPj + MiMj) / 4.0  
    # -----------------------------------------    
    if ((dxx * dyy - dxy * dxy) > 0):
        det = 1.0 / (dxx * dyy - dxy * dxy)
    else:
        det = 0.0
    
    ix = - (dyy * dx - dxy * dy) * det + max_j - float(length_x//2)    
    iy = - (dxx * dy - dxy * dx) * det + max_i - float(length_y//2)
    
    # vertical, then horizontal
    out[0] = iy
    out[1] = ix
    
    return out
