# SWaRP

Speckle Wavefront Reconstruction Package. Based on X-ray Speckle Tracking (XST) principle.

## How to install the package in Linux

git clone https://gitlab.esrf.fr/cojocaru/swarp

cd swarp

For python2 (first install python-pip):

pip install .

Alternative: sudo python setup.py install

For python3 (first install python3-pip):

python3 -m pip install .

## How to run the code (for now)

python waveFront.py image1 image2 file.ini

e.g. python waveFront.py ./resources/image1.edf ./resources/image2.edf ./resources/test_input.ini

python detectorDistortion.py path_to_files file.ini

e.g. python detectorDistortion.py ./resources/det-dist/ ./resources/test_input.ini

## Code manual:
https://www.eucall.eu/sites/sites_custom/site_eucall/content/e21597/e25317/e79563/EUCALL_WP7_PUCCA_Deliverable_7_8_D37_30_09_2018.pdf?preview=preview

## Authors

* **Ruxandra Cojocaru**, cojocaru@esrf.fr
* **Sebastien Berujon**, sebastien.berujon@esrf.fr

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

