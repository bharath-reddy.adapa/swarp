#/*##########################################################################
#
# SWaRP: Speckle Wavefront Reconstruction Package 
#
# Copyright (c) 2016-2018 European Synchrotron Radiation Facility
#
# This file is part of the SWaRP Speckle Wavefront Reconstruction Package
# developed at the ESRF by the staff of BM05 as part of EUCALL WP7:PUUCA.
#
# This project has received funding from the European Union’s Horizon 2020 
# research and innovation programme under grant agreement No 654220.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
#############################################################################*/

__author__ = ['Ruxandra Cojocaru', 'Sebastien Berujon']
__contact__ = 'cojocaru@esrf.fr; sebastien.berujon@esrf.fr'
__license__ = 'MIT'
__copyright__ = 'European Synchrotron Radiation Facility, Grenoble, France'
__date__ = '31/07/2018'

import os
import sys

try:
    from setuptools import setup
    print('imported setuptools -> setup')
except ImportError:
    from distutils.core import setup
    print('imported distutils.core -> setup')


def get_readme():
    _dir = os.path.dirname(os.path.abspath(__file__))
    long_description = ''
    with open(os.path.join(_dir, 'README.md')) as f:
        for line in f:
            if 'main_window' not in line:
                long_description += line
    return long_description

#~ def install_requires():
    #~ with open('requirements.txt') as fp:
        #~ install_requires = fp.readlines()
    #~ return install_requires
    

def get_requirements():
    requirements = list()
    with open('requirements.txt') as fp:
        for line in fp:
            if line.startswith('#') or line == '\n':
                continue
            line = line.strip('\n')
            requirements.append(line)
            
    print('install_requires: ' + str(requirements))
    
    return requirements



def main():
    """The main entry point."""
    if sys.version_info < (2, 7):
        sys.exit('swarp requires at least Python 2.7')
    #~ elif sys.version_info[0] == 3 and sys.version_info < (3, 4):
        #~ sys.exit('swarp requires at least Python 3.4')

    kwargs = dict(
        name='swarp',
        version='1.0.0',
        description='x-ray Speckle tracking WAveFront Reconstruction Package',
        long_description=open('README.md').read(),
        license='MIT',
        author='Ruxandra Cojocaru, Sebastien Berujon',
        author_email='cojocaru@esrf.fr, sebastien.berujon@esrf.fr',
        url='https://gitlab.esrf.fr/cojocaru/swarp',
        download_url='https://gitlab.esrf.fr/cojocaru/swarp',
        keywords='wavefront, speckle, x-ray, synchrotron',
        install_requires=get_requirements(),
        platforms=[
            'MacOS :: MacOS X',
            'Microsoft :: Windows',
            'POSIX :: Linux',
            ],
        packages=[
            'swarp',
            'swarp.resources',
            'swarp.tests',
            ],
        package_data={
            'swarp.resources': [
                'det_dist/*.edf',
                'disto_maps/img_distoH.dat',
                'disto_maps/img_distoV.dat',
                'flats/*.edf',
                'image1.edf',
                'image2.edf',
                'test_input.ini'
                ],
            },
        classifiers=[
            'Development Status :: 4 - Beta',
            'Environment :: X11 Applications :: Qt',
            'Intended Audience :: Science/Research',
            'License :: OSI Approved :: MIT License',
            'Natural Language :: English',
            'Operating System :: MacOS :: MacOS X',
            'Operating System :: Microsoft :: Windows',
            'Operating System :: POSIX :: Linux',
            'Programming Language :: Python :: 2.7',
            'Programming Language :: Python :: 3.4',
            'Topic :: Scientific/Engineering :: Physics',
            ],
        test_suite='nose.collector',
        tests_require=['nose']
        )

    # At the moment pip/setuptools doesn't play nice with shebang paths
    # containing white spaces.
    # See: https://github.com/pypa/pip/issues/2783
    #      https://github.com/xonsh/xonsh/issues/879
    # The most straight forward workaround is to have a .bat script to run
    # swarp on Windows.

    #~ if 'win32' in sys.platform:
        #~ kwargs['scripts'] = ['swarp/swarp.bat']
    #~ else:
        #~ kwargs['scripts'] = ['swarp/']

    setup(**kwargs)


if __name__ == '__main__':
    main()

